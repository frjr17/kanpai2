import { defineStyleConfig, extendTheme } from "@chakra-ui/react";
import { anzac } from "./colors";

export const mainTheme = extendTheme({
  colors: {
    anzac,
  },
  styles: {
    global: {
      ".react-datepicker-wrapper": {
        width: "100%",
      },
      ".react-datepicker__header": {
        background: "anzac.400",
        color: "white",
        border: "3px solid",
        borderColor: "anzac.400",
      },
      ".react-datepicker__current-month": {
        color: "white",
      },
      ".react-datepicker__day-name": {
        color: "white",
      },
      ".react-datepicker__navigation": {
        color: "white",
      },
      ".react-datepicker": {
        border: "1px solid",
        borderColor: "anzac.400",
        borderRadius: "md",
        width: "100%",
      },
      ".react-datepicker__aria-live": {
        display: "none",
      },
      ".react-datepicker__navigation-icon::before": {
        borderColor: "white"
      },
      input: {
        backgroundColor: "white !important",
      },
    },
  },
  components: {
    Button: defineStyleConfig({
      baseStyle: {
        fontSize: "sm",
      },
      variants: {
        kanpai: {
          backgroundColor: "anzac.400",
          color: "white",
          border: "2px solid",
          borderColor: "anzac.400",
          transition: "0.3s",
          lineHeight: 1,
          ":hover": {
            backgroundColor: "white",
            color: "anzac.400",
          },
          ":active": {
            backgroundColor: "anzac.400",
            color: "white",
          },
        },
        kanpaiOutlined: {
          backgroundColor: "white",
          color: "anzac.400",
          border: "2px solid anzac.400",
          transition: "0.3s",
          ":hover": {
            backgroundColor: "anzac.400",
            color: "white",
          },
          ":active": {
            backgroundColor: "white",
            color: "anzac.400",
          },
        },
      },
      defaultProps: {
        variant: "kanpai",
      },
    }),
    Text: defineStyleConfig({
      baseStyle: {
        fontSize: "sm",
        color: "blackAlpha.800",
      },
    }),
    Heading: defineStyleConfig({
      baseStyle: {
        color: "blackAlpha.800",
      },
    }),
    Link: defineStyleConfig({
      baseStyle: {
        color: "blackAlpha.800",
        ":hover": {
          textDecoration: "none",
        },
      },
    }),
  },
});
