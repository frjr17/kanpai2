import { initializeApp } from "firebase/app";
import { FacebookAuthProvider, getAuth, GoogleAuthProvider } from "firebase/auth";

/**Constant that returns a true if NODE_ENV variable is production*/
export const isProduction = process.env.NODE_ENV === "production";

/**Constant that returns the correct server url according to NODE_ENV environment.*/
export const serverUrl = isProduction ? process.env.NEXT_PUBLIC_SERVER_URL : process.env.NEXT_PUBLIC_SERVER_LOCAL_URL;

/**Firebase configuration for client */
const firebaseConfig = {
    apiKey: "AIzaSyCUcfSB-ttyvVJ8rVqKSyFM-JKErLYNdcE",
    authDomain: "kanpai-dcdec.firebaseapp.com",
    projectId: "kanpai-dcdec",
    storageBucket: "kanpai-dcdec.appspot.com",
    messagingSenderId: "451867122278",
    appId: "1:451867122278:web:c28b9c5986dd11fbc1aba7"
};

/**Firebase app initialization */
export const firebaseApp = initializeApp(firebaseConfig, 'frontend');

/**Firebase auth object */
export const auth = getAuth(firebaseApp);
/**Firebase google Authentication Provider */
export const googleProvider = () => new GoogleAuthProvider();
/**Firebase Facebook Authentication Provider */
export const facebookProvider = () => new FacebookAuthProvider();
