import { AiFillGift, AiFillPlusCircle } from "react-icons/ai";
import { FaBabyCarriage, FaUserGraduate } from "react-icons/fa";
import { GiBigDiamondRing, GiHolyWater } from "react-icons/gi";
import { HiUserCircle } from "react-icons/hi";
import { ImUserTie } from "react-icons/im";
import { IoMdBookmarks } from "react-icons/io";
import { IoSearchCircle } from "react-icons/io5";

/**Homepage banner images */
export const bannerImages = ["/images/banner/home-1.jpg", "/images/banner/home-2.jpg", "/images/banner/home-3.jpg", "/images/banner/home-4.jpg", "/images/banner/home-5.jpg", "/images/banner/home-6.jpg", "/images/banner/home-7.jpg", "/images/banner/home-8.jpg"];

/**Homepage 'howItWorks' section steps */
export const howItWorks = [
  {
    label: "REGÍSTRATE",
    description: "Rápido y seguro",
    icon: HiUserCircle,
  },
  {
    label: "BUSCA",
    description: "Descubre cientos de proveedores para cualquier tipo de evento",
    icon: IoSearchCircle,
  },
  {
    label: "COTIZA",
    description: "Compra precios y disponibilidad",
    icon: AiFillPlusCircle,
  },
  {
    label: "¡RESERVA!",
    description: "Desde donde quiera que estés",
    icon: IoMdBookmarks,
  },
];

/**Homepage event types. */
export const events = [
  {
    label: "Bodas",
    icon: GiBigDiamondRing,
  },
  {
    label: "Cumpleaños",
    icon: AiFillGift,
  },
  {
    label: "Evento Corporativo",
    icon: ImUserTie,
  },
  {
    label: "Infantiles",
    icon: FaBabyCarriage,
  },
  {
    label: "Bautizos",
    icon: GiHolyWater,
  },
  {
    label: "Graduaciones",
    icon: FaUserGraduate,
  },
];
