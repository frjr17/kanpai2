import { BsFilePost, BsMegaphoneFill } from "react-icons/bs";
import { FaFileContract, FaIdCard, FaTasks, FaUser, FaUsers } from "react-icons/fa";
import { AiFillBell } from "react-icons/ai";
import {SiWhatsapp} from 'react-icons/si'
import {FiHelpCircle} from 'react-icons/fi'
import { HiUsers } from "react-icons/hi";
import {GrBusinessService} from 'react-icons/gr'
import { ImStatsDots } from "react-icons/im";


export const menuItems = [
  {
    label: "Perfil",
    Icon: FaUser,
    href: "/perfil",
  },
  {
    label: "Usuarios",
    Icon: FaUsers,
    href: "/admin/usuarios",
  },
  {
    label: "Proveedores",
    Icon: FaIdCard,
    href: "/admin/proveedores",
  },
  {
    label: "Servicios",
    Icon: FaTasks,
    href: "/admin/servicios",
  },
  {
    label: "Notificaciones",
    Icon: AiFillBell,
    href: "/admin/notificaciones",
  },
];

export const providerMenuItems = [
  {
    label: "Perfil",
    Icon: FaUser,
    href: "/perfil",
  },
  {
    label: "Publicaciones",
    Icon: BsFilePost,
    href: "/publicacion/estadisticas",
  },
  {
    label: "Vincular con Whatsapp",
    Icon: SiWhatsapp,
    href: "/publicacion/estadisticas",
  },
  {
    label: "Contrato",
    Icon: FaFileContract,
    href: "/proveedor/contrato",
  },
  {
    label: "Terminos y Condiciones",
    Icon: FiHelpCircle,
    href: "/proveedor/contrato",
  },
]

export const adminMenuItems = [
  {
    label: "Usuarios",
    Icon: HiUsers,
    href: "/administrador/usuarios",
  },
  {
    label: "Proveedores",
    Icon: GrBusinessService,
    href: "/administrador/proveedores",
  },
  {
    label: "Estadísticas",
    Icon: ImStatsDots,
    href: "/administrador/estadisticas",
  },
]

/**Header nav items. */
export const NAV_ITEMS = [
  {
    label: "Panel",
    href: "/panel",
  },
  {
    label: "Conviértete en Proveedor",
    Icon: BsMegaphoneFill,
    href: "/proveedor/registro",
  },
];
