import { serverUrl } from "../constants";

/**Function that matches the serverUrl with the route passed.*/
export const serverApi = (url, outSideComponent = false) => outSideComponent ? serverUrl + url : `/api/${url}`;