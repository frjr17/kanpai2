import { NotificationManager } from "react-notifications";
import { ValidationError } from "yup";
import { create } from "zustand";
import { persist } from "zustand/middleware";
import CustomInput from "../../components/customInput";


/**Class that instance a fresh new state with zustand. */
export class State {
    constructor(name, initialState, Functions, options = {}) {
        this.name = name
        this.Functions = Functions;
        this.initialState = {
            isFetching: false,
            error: undefined,
            formErrors: {}, ...initialState
        };
        // Returning hook immediately
        return options.isPersist ? this.#createPersistState(options.persist) : this.#createPlainState(name);
    }

    /**Creates a zustand non-persisting state. */
    #createPlainState = () => {
        return create((set, get, store) => {
            const functions = new this.Functions(this.initialState, store);

            return {
                ...this.initialState,
                ...functions.getFunctions(),
            }
        })
    }

    /**Creates a zustand persisting state. */
    #createPersistState = (persistOptions = {}) => {
        return create(persist((set, get, store) => {
            const functions = new this.Functions(this.initialState, store);

            return {
                ...this.initialState,
                ...functions.getFunctions(),
            }
        }, { name: this.name, ...persistOptions }))
    }
}

/**Class that classifies and structure state sync and async functions. */
export default class GlobalStateFunctions {
    constructor(initialState, store) {
        this.state = store
        this._state = store
        this.set = store.setState;
        this.get = store.getState;
        this.initialState = initialState
    }

    // GetFormInputs
    getFormInputs = ({ order, inputs }, just = [], exclude = []) => {
        if (just.length) {
            order = order.filter(key => just.includes(key));
        }

        if (exclude.length) {
            order = order.filter(key => !exclude.includes(key));
        }
        return order.map((key) => {
            const formInput = inputs[key];

            formInput.onChange = this.handleChange(key);
            formInput.isDisabled = this.get().isFetching;
            formInput.value = this.get()[key];
            formInput.errors = this.get().formErrors[key];
            formInput.name = key

            return <CustomInput key={key} {...formInput} />
        })
    }


    // Catcher for async functions
    #catcher = async (code, props = {}) => {
        const { set } = this;

        // Preparing state without errors and fetching
        set({ error: undefined })
        set({ isFetching: true });

        // Running custom code and extracting details.
        let { success, message, error, noNotification, infoMessage, ...data } = await code(props);

        // If custom code has an info message, displaying it
        if (infoMessage) {
            NotificationManager.info(infoMessage)
        }

        if (success) {
            // Is custom code has a message without noNotification flag, displaying it.
            if (message && !noNotification) {
                NotificationManager.success(message)
            }

            // Setting data to state and finalizing fetching.
            set({ ...data })
            set({ isFetching: false });
        } else {
            // Setting error immeidately if success != true
            set({ error })

            // Verifying if backend shows validation error.
            if (error.errors && error.message === "Errores de validacion") {
                set({ formErrors: error.errors });
                noNotification = false
            }

            // Displaying error message if "noNotification" flag is false
            if (message && !noNotification) {
                NotificationManager.error(message)
            }

            // Finalizing fetching and throwing error.
            set({ isFetching: false });
            throw error
        }
        return data
    }

    /**Function that extract errors from yup frontend validator. */
    #extractYupErrors = (errors) => {
        /**Declaring object with errors extracted from validators "yup" string */
        const errorsCollected = {};

        // Iterating yup errors..
        errors.inner.forEach((error) => {
            // Seeing if "error.path" prop is not "undefined"
            if (error.path) {
                // If error path doesn't exist in errorsCollected, it will be created as an empty array
                if (!errorsCollected[error.path]) {
                    errorsCollected[error.path] = [];
                }
                // Adding error...
                errorsCollected[error.path].push(error.message);
            }
        });

        return errorsCollected;
    };

    /**Function that get all custom and basic functions from class and throws them to state.*/
    getFunctions = () => {
        // Getting excluded functions from class (this function included.)
        let toExclude = ['constructor', 'set', 'get', 'initialState', 'get', 'state', 'validate', '#extractYupErrors']
        toExclude = new Set(toExclude);

        // Getting all functions from class and filtering from the ones above.
        let allFunctionNames = Object.getOwnPropertyNames(this);
        allFunctionNames = allFunctionNames.filter(name => !toExclude.has(name))

        // Creating plain and async functions objects.
        const plainFunctions = {}
        const asyncFunctions = {};

        // Iterating
        allFunctionNames.forEach(functionName => {
            /**If the function is async, then returning a function with "props" object param
             * that returns the selected function wrapped in #catcher with all props passed down.
             */
            if (this[functionName].constructor.name === 'AsyncFunction') {
                asyncFunctions[functionName] = (props) => this.#catcher(this[functionName], props);
            } else {
                // If it's a sync function, then just returning the selected function.
                plainFunctions[functionName] = this[functionName]
            }
        })
        // Sending all plain and async functions into one object.
        return { ...plainFunctions, ...asyncFunctions }
    }

    /**Function that sets state, only for forms.*/
    handleChange = (name) => (event) => {
        this.deleteFormErrors(name)
        this.set({ [name]: event.target.value })
    }

    /**Deletes everything from database. */
    deleteEverything = (...but) => {
        const state = this.get();
        if (this._state.persist) {
            // Clearing all storage from state.
            this._state.persist.clearStorage();
        }

        // If there's no exluding param, deleting everything...
        if (!(but.length)) {
            return this.set({ ...this.initialState });
        }

        // Excluding functions
        let dataEntries = Object.entries(state).filter(([key]) => typeof state[key] !== 'function');

        // Erasing all non-excluded state params.
        dataEntries = dataEntries.map(([key, value]) => !but.includes(key) ? [key, this.initialState[key]] : [key, value])
        const data = Object.fromEntries(dataEntries)
        // Setting new state.
        return this.set({ ...data });
    }

    /**Adds errors to form.*/
    addFormErrors = (errors) => this.set({ formErrors: { ...errors } });

    /**Function that uses yup frontend validator to validate forms. */
    validate = async (dataToValidate, validator) => {
        try {
            // Validating Login Data
            const data = await validator.validate(dataToValidate, { abortEarly: false });
            // Setting state if there's set prop
            this.set({ ...data });
        } catch (errors) {
            if (errors instanceof ValidationError) {
                // Extracting yup errors.
                const errorsCollected = this.#extractYupErrors(errors);

                // Adding all form errors to state.
                this.get().addFormErrors(errorsCollected);

                // Throwing error to stop form progess...
                throw "Validation Error";
            }
        }
    };

    /**Function that deletes form errors */
    deleteFormErrors = (...toInclude) => {
        // If there's no args, it will erase every form error.
        if (!toInclude.length) {
            this.set({ formErrors: {} });
            return;
        }

        // Getting all form error and spreading it into a new object.
        const formErrors = this.get().formErrors;
        const errors = { ...formErrors };

        // Iterating errors
        Object.entries(errors).forEach(([errorName]) => {
            // If errorName is included in "toInclude" prop, then all errors will be erased.
            if (toInclude.includes(errorName)) {
                errors[errorName] = undefined;
            }
        });

        // Setting up the new errors
        this.set({ formErrors: { ...errors } });
    };
}