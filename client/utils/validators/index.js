import { object, string } from "yup";

/**Custom validator for password.*/
export const passwordValidatorParams = string().min(8, "La contraseña debe tener 8 caracteres mínimo.").matches(new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$"), "Las contraseñas deben tener un caracter mayúscula, un carater minúscula, un número y un símbolo.").required("Este campo es requerido.");

/**Custom validator for email */
export const emailValidatorParams = string().email("Esto no es una direccion de correo. (Ej.: example@gmail.com)").required("Tienes que escribir tu correo").lowercase().trim();

export const emailValidator = object({
  email: emailValidatorParams,
});

export const passwordValidator = object({
  password: passwordValidatorParams,
});

