import { useState } from "react";

export const useProfileState = (userState) => {
    const [personalState, setPersonalState] = useState({
        name: userState.name,
        lastName: userState.lastName,
        phoneNumber: userState.phoneNumber,
        state: userState.state,
        city: userState.city,
    });

    const [passwordState, setPasswordState] = useState({
        currentPassword: userState.currentPassword,
        newPassword: userState.newPassword,
    });

    const changeState = (newUserState, isPersonalState = true) => {
        if (isPersonalState) {

            setPersonalState({
                name: newUserState.name,
                lastName: newUserState.lastName,
                phoneNumber: newUserState.phoneNumber,
                state: newUserState.state,
                city: newUserState.city,
            });
        }

        setPasswordState({
            currentPassword: newUserState.currentPassword,
            newPassword: newUserState.newPassword,
        });
    };

    return { personalState, passwordState, changeState };
};
