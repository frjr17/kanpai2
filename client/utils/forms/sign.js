export const signInForm = {
    order: ['email', 'password', 'captchaToken'],
    inputs: {
        email: { label: "Email", type: "text", isRequired: true },
        password: { label: "Contraseña", type: "password", isRequired: true },
        captchaToken: { type: "captcha" },
    }
};

export const signUpForm = {
    order: ['name', 'lastName', 'email', 'password', 'confirmPassword', 'captchaToken'],
    inputs: {
        name: { label: "Nombre", type: "text", isRequired: true },
        lastName: { label: "Apellido", type: "text", isRequired: true },
        email: { label: "Correo", type: "text", isRequired: true },
        password: { label: "Contraseña", type: "password", isRequired: true },
        confirmPassword: { label: "Confirmar Contraseña", type: "password", isRequired: true },
        captchaToken: { type: "captcha" },
    }
};
