
export const providerProfileForm = {
    order: ['hasRfc', 'rfcType', 'rfc', 'curp', 'state', 'city', 'businessMayorsOffice', 'postalCode', 'delegateName', 'delegatePhone', 'delegateEmail',],
    inputs: {
        hasRfc: { type: "boolean", label: "¿Tienes RFC?", isRequired: true, styles: { margin: 3, width: { base: "100%", md: '30%' } } },
        rfcType: { type: "radio", radioInputs: ['Física', 'Moral'], label: "Tipo de RFC", isRequired: true, styles: { margin: 3, width: { base: "100%", md: '30%' } } },
        rfc: { type: "text", label: "Escribe tu RFC", styles: { width: { base: "100%", md: '30%' }, margin: 3 } },
        curp: { type: "text", label: "Escribe tu CURP", isRequired: true, styles: { width: { base: "100%", md: '30%' }, margin: 3 } },
        city: { type: "select", label: "Ciudad", isRequired: true, styles: { width: { base: "100%", md: '30%' }, margin: 3 } },
        state: { type: "select", label: "Estado", isRequired: true, styles: { width: { base: "100%", md: '30%' }, margin: 3 } },
        postalCode: { type: "number", label: "Código Postal", isRequired: true, styles: { width: { base: "100%", md: '30%' }, margin: 3 } },
        delegateEmail: { type: "text", label: "Correo del Titular", isRequired: true, styles: { width: { base: "100%", md: '30%' }, margin: 3 } },
        delegateName: { type: "text", label: "Nombre del Titular", isRequired: true, styles: { width: { base: "100%", md: '30%' }, margin: 3 } },
        delegatePhone: { type: "text", label: "Teléfono del Titular", isRequired: true, styles: { width: { base: "100%", md: '30%' }, margin: 3 } },
        businessMayorsOffice: { type: 'select', label: "Alcaldía", isRequired: true, styles: { width: { base: "100%", md: '30%' }, margin: 3 } },
    }
}



export const businessForm = {
    order: ['businessName', 'businessEmail', 'businessPhone', 'businessLine'],
    inputs: {
        businessName: { type: "text", label: "Nombre del Negocio", isRequired: true, styles: { width: { base: "100%", md: '30%' }, margin: 3 } },
        businessEmail: { type: "text", label: "Correo del Negocio", isRequired: true, styles: { width: { base: "100%", md: '30%' }, margin: 3 } },
        businessPhone: { type: 'phone', label: "Teléfono del Negocio", isRequired: true, styles: { width: { base: "100%", md: '30%' }, margin: 3 } },
        businessLine: { type: "text", label: "Giro del Negocio", isRequired: true, styles: { width: { base: "100%", md: '30%' }, margin: 3 } },
    }
}

