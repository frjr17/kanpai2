export const personalForm = [
    { label: "Nombre", type: "text", name: "name", isRequired: true },
    { label: "Apellido", type: "text", name: "lastName", isRequired: true },
    { label: "Teléfono", leftAddonChildren: "+52", placeholder: "El numero debe tener de 8 a 10 dígitos", type: "text", name: "phoneNumber" },
    { label: "Estado", type: "text", name: "state" },
    { label: "Ciudad/municipio", type: "text", name: "city" },
];

export const securityForm = [
    { label: "Contraseña Actual", type: "password", name: "currentPassword", isRequired: true },
    { label: "Nueva Contraseña", type: "password", name: "newPassword", isRequired: true },
];
