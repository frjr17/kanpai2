import { Box, ButtonGroup, Container, IconButton, Link, SimpleGrid, Stack } from "@chakra-ui/react";
import { BsFacebook, BsInstagram, BsTwitter } from "react-icons/bs";
import KanpaiLogo from "../../components/kanpaiLogo";
import FooterListHeader from "./footerListHeader";
import NextLink from "next/link";

export default function Footer() {
  return (
    <Box sx={styles.container}>
      <Container as={Stack} sx={styles.stackContainer}>
        <SimpleGrid templateColumns={{ sm: "1fr 1fr", md: "2fr 1fr 1fr 1fr 1fr" }} spacing={8}>
          {/* First column => Kanpai Logo */}
          <Stack spacing={6}>
            <Box>
              <KanpaiLogo width={150} height={150} />
            </Box>
            <Stack align={"flex-start"}>
              <Link href={"#"}>Empleo</Link>
              <Link href={"#"}>Noticias</Link>
              <Link href={"#"}>Políticas</Link>
              <Link href={"#"}>Ayuda</Link>
              <Link href={"#"}>Diversidad e Inclusión</Link>
              <Link href={"#"}>Datos de la empresa</Link>
            </Stack>
          </Stack>
          {/* Second Column */}
          <Stack align={"flex-start"}>
            <FooterListHeader>Descubre KANPAI</FooterListHeader>
            <Link href={"#"}>¿Por qué utilizar KANPAI?</Link>
            <Link href={"#"}>Confianza y Seguridad</Link>
            <Link href={"#"}>Descuentos para planners</Link>
            <FooterListHeader>Ayuda</FooterListHeader>
            <Link href={"#"}>FAQs</Link>
            <Link href={"#"}>Contáctanos</Link>
          </Stack>
          {/* Third Column */}
          <Stack align={"flex-start"}>
            <FooterListHeader>Conviértete en proveedor</FooterListHeader>
            <Link as={NextLink} href={"/proveedores/registro"}>
              Inscríbete como proveedor
            </Link>
            <Link href={"#"}>Sube un anuncio</Link>
          </Stack>
          {/* Fourth Column */}
          <Stack align={"flex-start"}>
            {/* Social Media Buttons */}
            <ButtonGroup>
              <IconButton aria-label="Instagram" icon={<BsInstagram />} />
              <IconButton aria-label="Twitter" icon={<BsTwitter />} />
              <IconButton aria-label="Facebook" icon={<BsFacebook />} />
            </ButtonGroup>
            <Link href={"#"}>Términos y Condiciones</Link>
            <Link href={"#"}>Privacidad</Link>
            <Link href={"#"}>Mapa de sitios</Link>
          </Stack>
        </SimpleGrid>
      </Container>
    </Box>
  );
}

const styles = {
  container: {
    background: "white",
    color: "gray.700",
  },
  stackContainer: {
    maxWidth: "6xl",
    paddingY: 10,
  },
};
