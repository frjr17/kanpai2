import { Box, Link, Stack, Icon } from "@chakra-ui/react";
import { NAV_ITEMS } from "../../../utils/constants/components";

/**Component that displays NavItems in Desktop environment*/
export default function DesktopNav() {
  return (
    <Stack direction={"row"} spacing={4}>
      {NAV_ITEMS.map((navItem) => (
        <Box key={navItem.label}>
          <Link href={navItem.href ?? "#"} sx={styles.link}>
            {navItem.Icon && <Icon as={navItem.Icon} color={"anzac.400"} mr={2} />}
            {navItem.label}
          </Link>
        </Box>
      ))}
    </Stack>
  );
}

const styles = {
  link: {
    fontSize: "sm",
    fontWeight: 500,
  },
};
