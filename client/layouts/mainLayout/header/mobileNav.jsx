import { Stack } from "@chakra-ui/react";
import { NAV_ITEMS } from "../../../utils/constants/components";
import MobileNavItem from "./mobileNavItem";

/**Component that displays navs in mobile nav environment. */
export default function MobileNav() {
  return (
    <Stack sx={styles.container}>
      {NAV_ITEMS.map((navItem) => (
        <MobileNavItem key={navItem.label} {...navItem} />
      ))}
    </Stack>
  );
}

const styles = {
  container: {
    background: "white",
    padding: 4,
    display: { md: "none" },
  },
};
