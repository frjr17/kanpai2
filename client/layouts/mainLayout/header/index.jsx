import { Box, Flex, IconButton, Stack, Collapse, useDisclosure } from "@chakra-ui/react";
import { HamburgerIcon, CloseIcon } from "@chakra-ui/icons";
import NextLink from "next/link";
import DesktopNav from "./desktopNav";
import MobileNav from "./mobileNav";
import KanpaiLogo from "../../../components/kanpaiLogo";
import dynamic from "next/dynamic";
import LoadingSpinner from "../../../components/loadingSpinner";
const AuthNavField = dynamic(() => import("../../../components/authNavField"), { ssr: false, loading: () => <LoadingSpinner /> });

/**Component that displays the main layout header */
export default function Header() {
  // Using useDisclosure hook for handling hamburger responsive icons in mobile environment
  const { isOpen, onToggle } = useDisclosure();
  return (
    <Box>
      <Flex sx={styles.container}>
        {/* Hamburger Responsive Icon for managing tab in mobile */}
        <Flex sx={styles.responsiveIconContainer}>
          <IconButton onClick={onToggle} icon={isOpen ? <CloseIcon w={3} h={3} /> : <HamburgerIcon w={5} h={5} />} variant={"ghost"} aria-label={"Toggle Navigation"} />
        </Flex>
        {/* Logo */}
        <Flex as={NextLink} href={"/"} sx={styles.imageContainer}>
          <KanpaiLogo width={100} height={40} />
        </Flex>
        {/* Nav part */}
        <Stack height={"50px"} align={"center"} justify={"flex-end"} direction={"row"} spacing={6}>
          {/* Desktop environment navs*/}
          <Flex sx={styles.desktopNavContainer}>
            <DesktopNav />
          </Flex>
          {/*Auth Nav buttons / User header profile*/}
          <AuthNavField />
        </Stack>
      </Flex>
      {/* Adding mobile environment navbar. */}
      <Collapse in={isOpen} animateOpacity>
        <MobileNav />
      </Collapse>
    </Box>
  );
}

const styles = {
  container: {
    justifyContent: "space-between",
    bg: "white",
    minH: "60px",
    paddingY: { base: 2 },
    paddingX: { base: 5, md: 10 },
    alignItems: "center",
  },
  responsiveIconContainer: {
    marginLeft: { base: 2 },
    display: { base: "flex", md: "none" },
  },
  imageContainer: {
    justify: { base: "center", md: "start" },
  },
  desktopNavContainer: {
    display: { base: "none", md: "flex" },
    marginLeft: 10,
    height: "full",
    alignItems: "center",
  },
  signInLink: {
    margin: 0,
    padding: 2,
    fontSize: "sm",
    fontWeight: 500,
  },
  signUpButton: {
    display: { base: "none", md: "inline-flex" },
    fontSize: "sm",
    fontWeight: 600,
    color: "white",
  },
  justNotMobile: {
    display: { base: "none", md: "flex" },
  },
};
