import { Link, Flex, Text, Icon } from "@chakra-ui/react";

/**Component that renders one navItem in mobile environment. */
export default function MobileNavItem({ label, href, Icon: NavIcon }) {
  return (
    <Flex as={Link} href={href ?? "#"} sx={styles.navContainer}>
      <Text sx={styles.nav}>
        {NavIcon && <Icon as={NavIcon} color={"anzac.400"} mr={2} />}
        {label}
      </Text>
    </Flex>
  );
}

const styles = {
  navContainer: {
    paddingY: 2,
    justifyContent: "space-between",
    alignItems: "center",
  },
  nav: {
    fontWeight: 600,
    color: "gray.600",
  },
  collapse: {
    marginTop: 0,
  },
};
