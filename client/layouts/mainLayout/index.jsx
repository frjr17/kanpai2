import { Box } from "@chakra-ui/react";
import Footer from "./footer";
import Header from "./header";
import React, { useEffect } from "react";
import { useAuthState } from "../../state/auth";
/**Higher Order Component (HOC) that wraps children into the main layout.*/
export default function MainLayout(props) {
  const authState = useAuthState();
  const { token } = authState

  useEffect(() => {
    validateToken();

    /*Adding token validation, if there's one, to verify if user is still logged in.
    If it's not, deleting everything from storage. */
    async function validateToken() {
      try {
        if (token) {
          await authState.verifyLoginToken();
        }
      } catch (error) { }
    }
  }, [token]);

  return (
    <Box sx={styles.container}>
      <Header />
      <Box minH={"75vh"}>{props.children}</Box>
      <Footer />
    </Box>
  );
}

const styles = {
  container: {
    background: "blackAlpha.100",
  },
};
