import React from "react";
import { Text } from "@chakra-ui/react";

/**Component that renders footer header. */
export default function FooterListHeader({ children }) {
  return (
    <Text fontWeight={"500"} fontSize={"lg"} mb={2}>
      {children}
    </Text>
  );
}
