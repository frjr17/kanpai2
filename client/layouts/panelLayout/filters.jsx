import { FormControl, FormLabel, Heading, Input, Select, VStack } from "@chakra-ui/react";
import { useState } from "react";
import ReactDatePicker from "react-datepicker";
import NumberInput from "../../components/numberInput";

/**Function that displays user filters. */
export default function Filters() {
  const [startDate, setStartDate] = useState(new Date());

  return (
    <VStack spacing={5} sx={styles.container}>
      <Heading as={"h4"} sx={styles.heading}>
        ¿Qué servicio estás buscando?
      </Heading>
      <FormControl>
        <FormLabel>Tipo de Servicio</FormLabel>
        <Select></Select>
      </FormControl>
      <FormControl>
        <FormLabel>Sub Servicio</FormLabel>
        <Select></Select>
      </FormControl>
      <FormControl>
        <FormLabel>Lugar</FormLabel>
        <Select></Select>
      </FormControl>
      <FormControl>
        <FormLabel>Fecha</FormLabel>
        <ReactDatePicker customInput={<Input />} selected={startDate} onChange={(date) => setStartDate(date)} />
      </FormControl>
      <FormControl sx={styles.numberInputFormControl}>
        <FormLabel sx={styles.numberInputFormLabel}>Personas</FormLabel>
        <NumberInput />
      </FormControl>
      <FormControl sx={styles.numberInputFormControl}>
        <FormLabel sx={styles.numberInputFormLabel}>Horas</FormLabel>
        <NumberInput />
      </FormControl>
    </VStack>
  );
}

const styles = {
  container: {
    marginTop: 5,
    padding: 5,
  },
  heading: {
    fontSize: "xl",
    textAlign: "center",
  },
  numberInputFormControl: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  numberInputFormLabel: {
    margin: 0,
    marginRight: 3,
  },
};
