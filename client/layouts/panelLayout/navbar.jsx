import { Flex, HStack, IconButton, Link, Icon } from "@chakra-ui/react";
import dynamic from "next/dynamic";
import { FiBell, FiMenu } from "react-icons/fi";
import KanpaiLogo from "../../components/kanpaiLogo";
import NextLink from "next/link";
import LoadingSpinner from "../../components/loadingSpinner";
import { NAV_ITEMS } from "../../utils/constants/components";
const AuthNavField = dynamic(() => import("../../components/authNavField"), { ssr: false, loading: () => <LoadingSpinner /> });

/**Component that renders the panel layout navbar.*/
export default function Navbar({ onOpen, ...rest }) {
  return (
    <Flex sx={styles.container} {...rest}>
      <IconButton sx={styles.justMobile} onClick={onOpen} variant="outline" aria-label="open menu" icon={<FiMenu />} />

      <Link as={NextLink} justifySelf={"center"} href={"/"} sx={styles.justMobile}>
        <KanpaiLogo width={100} height={40} />
      </Link>

      <HStack display={{ base: "none", md: "flex" }} spacing={{ base: "0", md: 6 }}>
        {NAV_ITEMS.map(
          (item) =>
            item.label !== "Panel" && (
              <Link fontSize={"sm"} key={"item.label"} as={NextLink} href={item.href} fontWeight={500}>
                <Icon as={item.Icon} w={3} h={3} mr={2} color={"anzac.400"} />
                {item.label}
              </Link>
            )
        )}
        <IconButton size="lg" variant="ghost" aria-label="open menu" icon={<FiBell />} />
        <Flex alignItems={"center"}>
          <AuthNavField />
        </Flex>
      </HStack>
    </Flex>
  );
}

const styles = {
  container: {
    position: "sticky",
    top: 0,
    zIndex: 3,
    paddingX: 4,
    height: "60px",
    alignItems: "center",
    background: "white",
    borderBottomWidth: 1,
    borderBottomColor: "gray.200",
    justifyContent: { base: "center", md: "flex-end" },
  },
  justMobile: {
    display: { base: "flex", md: "none" },
    justifySelf: 'center'
  },
  justNotMobile: {
    display: { base: "none", md: "flex" },
  },
};
