import { Box, CloseButton, Flex, HStack, IconButton, Link, Icon, VStack } from "@chakra-ui/react";
import KanpaiLogo from "../../components/kanpaiLogo";
import Filters from "./filters";
import NextLink from "next/link";
import { NAV_ITEMS } from "../../utils/constants/components";
import dynamic from "next/dynamic";
import LoadingSpinner from "../../components/loadingSpinner";
import { FiBell } from "react-icons/fi";
const AuthNavField = dynamic(() => import("../../components/authNavField"), { ssr: false, loading: () => <LoadingSpinner /> });

/**Component that displays panel layout sidebar with filters, kanpai logo and a close button in mobile. */
export default function SidebarContent({ onClose, ...rest }) {
  const sx = { ...styles.container, ...rest.sx };
  return (
    <Box sx={sx}>
      <Flex sx={styles.sidebarContainer}>
        <Link as={NextLink} href={"/"}>
          <KanpaiLogo width={100} height={40} />
        </Link>

        <CloseButton display={{ base: "flex", md: "none" }} onClick={onClose} />
      </Flex>
      <VStack display={{ base: "flex", md: "none" }} align={'center'}>
        <HStack spacing={{ base: "0", md: 6 }}>

          <IconButton size="lg" variant="ghost" aria-label="open menu" icon={<FiBell />} />
          <Flex alignItems={"center"}>
            <AuthNavField />
          </Flex>
        </HStack>
        {NAV_ITEMS.map(
          (item) =>
            item.label !== "Panel" && (
              <Link fontSize={"sm"} key={"item.label"} as={NextLink} href={item.href} fontWeight={500}>
                <Icon as={item.Icon} w={3} h={3} mr={2} color={"anzac.400"} />
                {item.label}
              </Link>
            )
        )}
      </VStack>
      <Filters />
    </Box>
  );
}

const styles = {
  container: {
    transition: "3s ease",
    background: "white",
    borderRight: "1px",
    borderRightColor: "gray.200",
    width: { base: "full", md: 80 },
    position: "sticky",
    top: 0,
    height: "100vh",
  },
  sidebarContainer: {
    height: 20,
    alignItems: "center",
    marginX: 8,
    justifyContent: "space-between",
  },
};
