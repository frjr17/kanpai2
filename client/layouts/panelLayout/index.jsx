import React, { useEffect } from "react";
import { Box } from "@chakra-ui/react";
import Footer from "../mainLayout/footer";
import Panel from "./panel";
import { useAuthState } from "../../state/auth";
/**Main Panel HOC */
export default function PanelLayout({ children }) {
  const authState = useAuthState();
  const { token } = authState;
  useEffect(() => {
    /*Adding token validation, if there's one, to verify if user is still logged in.
    If it's not, deleting everything from storage. */

    validateToken();

    async function validateToken() {
      try {
        if (token) {

          await authState.verifyLoginToken();
        }

      } catch (error) { }
    }
  }, [token]);
  return (
    <Box>
      <Panel>{children}</Panel>
      <Footer />
    </Box>
  );
}
