import { Box, Drawer, DrawerContent, useDisclosure } from "@chakra-ui/react";
import MobileNav from "./navbar";
import SidebarContent from "./sidebarContent";

/**Higher Order Componet (HOC) that wraps children into Panel Layout*/
export default function Panel({ children }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <Box sx={styles.container}>
      <SidebarContent onClose={() => onClose} sx={styles.sidebarContent} />
      <Drawer autoFocus={false} isOpen={isOpen} placement="left" onClose={onClose} returnFocusOnClose={false} onOverlayClick={onClose} size="full">
        <DrawerContent>
          <SidebarContent onClose={onClose} />
        </DrawerContent>
      </Drawer>
      <Box sx={styles.mobileNavContainer}>
        {/* mobilenav */}
        <MobileNav onOpen={onOpen} />
        <Box sx={styles.children}>{children}</Box>
      </Box>
    </Box>
  );
}

const styles = {
  container: {
    display: "flex",
    minHeight: "100vh",
    background: "gray.100",
  },
  sidebarContent: {
    display: { base: "none", md: "block" },
  },
  mobileNavContainer: {
    width: "full",
  },
  children: {
    padding: 4,
  },
};
