import { Center, Divider, Heading, VStack } from "@chakra-ui/react";
import { useEffect } from "react";
import { captchaRef } from "../../components/customInput/recaptcha";
import KanpaiLogo from "../../components/kanpaiLogo";
import { useAuthState } from "../../state/auth";

export default function SignLayout(props) {
  // Calling authorization state
  const authState = useAuthState();

  useEffect(() => {
    // If there's some captcha reference, adding it to state...
    if (captchaRef.current) {
      authState.setCaptchaRef(captchaRef);
    }
  }, [captchaRef]);

  return (
    <Center background={"url(/images/backgrounds/wedding.jpg)"} backgroundSize={"cover"} minHeight={"100vh"} width={"100%"} paddingY={"20px"}>
      <VStack spacing={5} padding={{ base: 5, md: 10 }} background={"white"} width={{ base: '95%', md: "50%" }} height={"fit-content"} rounded={"md"}>
        <VStack spacing={0}>
          <KanpaiLogo width={200} height={100} />
          <Heading>{props.title}</Heading>
        </VStack>
        <Divider width={"80%"} />
        {props.children}
      </VStack>
    </Center>
  );
}
