<img src="./../public/images/logo/logo%20-%20s.png" alt="drawing" width="100"/>

# **_Kanpai Project - Layouts_** 🔥

Layouts means all the design structures that are repeatedly used. This folder contains the main macro design styles for the app pages. Also, it cares about SEO and SSR.

## **Layouts Available**

1. **Main Layout**: The most important of all, used in 404 pages and in [Home Page](../pages/index.jsx)
2. **Panel Layout**: This layout is used when user enters to the dashboard of products and services.
3. **Sign Layout**: This layout handles all the sign processes.