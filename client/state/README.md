<img src="../public/images/logo/logo%20-%20s.png" alt="drawing" width="100"/>

# **_Kanpai Project - State_** ⚒

This is the heart of the app. State folder manages all client-side data, and manages its flow through the app as well.

State management in this app is provided by [Zustand Package](https://github.com/pmndrs/zustand). And tweaked by some [functions](../utils/functions/state.js) to make it more dynamic.

Each state folder consists of 3 files:

1. **Index.js**: This file stores the entire application state, and its exported for the rest of the app.
2. **functions.js**: In this file, all functions (sync or async) are included.
3. **validators.js**: If the state needs some validation, here it's counted.
