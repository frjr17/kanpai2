import { State } from "../../utils/functions/state"
import AdminStateFunctions from "./functions"

const initialState = {
    users: [],
    providers: [],
}

export const useAdminState = new State('AdminState', initialState, AdminStateFunctions)