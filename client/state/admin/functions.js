import axios, { AxiosError } from "axios";
import { serverApi } from "../../utils/functions";
import GlobalStateFunctions from "../../utils/functions/state";

export default class StateFunctions extends GlobalStateFunctions {
    constructor(initialState, store) {
        super(initialState, store);
    }

    getProviders = async (props) => {
        const result = {};

        try {
            // Fetching data from backend
            const axiosResponse = await axios({
                method: "GET",
                url: serverApi('/provider/all'),
                headers: {
                    Authorization: props.token
                }
            })
            const responseData = axiosResponse.data;
            result.providers = responseData.providers;
            result.message = responseData.message
            result.noNotification = true
            result.success = true;
        } catch (error) {
            // Adding error params to final obj...
            result.error = error;
            result.message = error.message;

            if (error instanceof AxiosError) {
                // console.error('getProvidersError',error)
                result.error = error.response.data;
                result.message = error.response.data.message;
            }
        }

        return result;
    }

}