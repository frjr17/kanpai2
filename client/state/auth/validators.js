import { object, ref, string } from "yup";
import { emailValidatorParams, passwordValidatorParams } from "../../utils/validators";

/**Validator for Sign In form */
export const signInValidator = object({
  email: emailValidatorParams,
  password: passwordValidatorParams,
  captchaToken: string().required("Tienes que escribir el captcha"),
});

/**Validator for Sign Up form */
export const signUpValidator = object({
  name: string().required("Tienes que escribir tu nombre"),
  lastName: string().required("Tienes que escribir tu apellido"),
  email: emailValidatorParams,
  password: passwordValidatorParams,
  confirmPassword: passwordValidatorParams.when("password", (password, schema) => {
    return schema.oneOf([ref("password")], "Las contraseñas deben coincidir");
  }),
  captchaToken: string().required("Tienes que escribir el captcha"),
});


export const resetPasswordValidator = object({
  password: passwordValidatorParams,
  confirmPassword: passwordValidatorParams.when("password", (password, schema) => {
    return schema.oneOf([ref("password")], "Las contraseñas deben coincidir");
  }),
})