import axios, { AxiosError } from 'axios';
import { FirebaseError } from 'firebase/app';
import { deleteUser, signInWithPopup } from 'firebase/auth';
import { auth, facebookProvider, googleProvider } from '../../utils/constants';
import { signInForm, signUpForm } from '../../utils/forms/sign';
import { serverApi } from '../../utils/functions';
import GlobalStateFunctions from '../../utils/functions/state';
import { emailValidator } from '../../utils/validators';
import { useUserState } from '../user';
import { resetPasswordValidator, signInValidator, signUpValidator } from './validators'

export default class AuthStateFunctions extends GlobalStateFunctions {
    constructor(initialState, store) {
        super(initialState, store);

    }

    // loginForm
    LoginForm = () => {
        return this.getFormInputs(signInForm)
    }

    // registerForm
    RegisterForm = () => {
        return this.getFormInputs(signUpForm)
    }

    // ResetPasswordRequest form
    ResetPasswordRequestForm = () => {
        signUpForm.inputs.email.placeholder = 'Escribe tu correo'
        return this.getFormInputs(signUpForm, ['email'])
    }

    // ResetPassword form
    ResetPasswordForm = () => {
        signUpForm.inputs.password.placeholder = 'Escribe la nueva contraseña'
        signUpForm.inputs.confirmPassword.placeholder = 'Confirma la nueva contraseña'
        return this.getFormInputs(signUpForm, ['password', 'confirmPassword'])
    }

    // Adding captchaReference to state...
    setCaptchaRef = (captchaRef) => this.set({ captchaRef })

    // Reset captcha
    resetCaptcha = () => {
        this.get().captchaRef?.current ? this.get().captchaRef.current.reset() : null
        this.set({ captchaToken: "" })
    }


    // verify resetPassword token
    verifyResetPasswordToken = async (props) => {
        const result = {};

        try {
            // verifying
            await axios({
                method: "GET",
                url: serverApi(`/auth/verify-reset-password/${props.token}`)
            })

            // catcher success details
            result.success = true;
        } catch (error) {
            // assigning error immediately
            result.error = error;
            result.message = error.message;

            if (error instanceof AxiosError) {
                // console.log('verifyResetPasswordTokenError',{error})

                // assigning error if its an AxiosError instance
                result.error = error.response.data;
                result.message = error.response.data.message;
                result.noNotification = true;
            }
        }

        return result
    }

    // verify login token
    verifyLoginToken = async () => {
        const result = {};
        const state = this.get();
        const userState = useUserState.getState();
        try {
            const axiosResponse = await axios({
                method: "GET",
                url: serverApi(`/auth/verify/${state.token}`)
            })
            const responseData = axiosResponse.data

            userState.setUser({ ...responseData.user })

            result.success = true;
        } catch (error) {
            state.deleteEverything();
            userState.deleteEverything();

            result.error = error;
            result.message = error.message;
            if (error instanceof AxiosError) {
                // console.log('verifyLoginTokenError',{error})
                result.error = error.response.data;
                result.message = error.response.data.message;
            }
        }

        return result
    }

    // register
    register = async () => {
        const result = {};

        try {
            const response = await axios({
                method: "POST",
                url: serverApi('/auth/register'),
                data: {
                    name: this.get().name,
                    lastName: this.get().lastName,
                    email: this.get().email,
                    password: this.get().password,
                    confirmPassword: this.get().confirmPassword,
                    captchaToken: this.get().captchaToken,
                }
            })

            // Showing register message
            const responseData = response.data;
            if (responseData.message) {
                result.infoMessage = responseData.message
            }

            result.success = true
        } catch (error) {
            result.error = error;
            result.message = error.message;
            if (error instanceof AxiosError) {
                result.error = error.response.data;
                result.message = error.response.data.message;
            }
        }

        return result
    }

    // validate register data
    validateRegisterData = () => {
        const registerData = {
            name: this.get().name,
            lastName: this.get().lastName,
            email: this.get().email,
            password: this.get().password,
            confirmPassword: this.get().confirmPassword,
            captchaToken: this.get().captchaToken
        }

        return this.validate(registerData, signUpValidator)
    }


    // firebase register
    firebaseRegister = async (props) => {
        const result = {};
        const { isGoogle } = props;
        let user;

        try {
            const provider = isGoogle ? googleProvider() : facebookProvider()
            const firebaseResponse = await signInWithPopup(auth, provider)
            const token = await auth.currentUser?.getIdToken(false);
            user = firebaseResponse.user;


            const axiosResponse = await axios({
                method: "POST",
                url: serverApi('/auth/register'),
                data: {
                    email: user.email,
                    name: user.displayName?.split(" ")[0],
                    lastName: user.displayName?.split(" ")[1],
                    profileImg: user.photoURL,
                    [isGoogle ? "isGoogleAccount" : "isFacebookAccount"]: true,
                    token: token
                }

            })
            const responseData = axiosResponse.data
            result.token = token;
            if (responseData.message) {
                result.message = responseData.message
            }

            result.success = true;
        } catch (error) {
            if (user) {
                await deleteUser(user);
            }
            result.error = error;
            result.message = error.message;
            if (error instanceof AxiosError) {
                // console.log('firebaseRegisterError',{error})
                result.error = error.response.data;
                result.message = error.response.data.message;
            }
        }

        return result
    }

    // login
    login = async () => {
        const result = {};

        try {
            const axiosResponse = await axios({
                method: "POST",
                url: serverApi("/auth/login"),
                data: {
                    email: this.get().email,
                    password: this.get().password,
                    captchaToken: this.get().captchaToken,

                }
            })

            const responseData = axiosResponse.data;
            result.token = responseData.token;

            result.success = true
        } catch (error) {
            result.error = error;
            result.message = error.message;
            if (error instanceof AxiosError) {
                // console.log('loginError',{error})
                result.error = error.response.data;
                result.message = error.response.data.message;
            }
        }

        return result
    }

    // validate login data
    validateLoginData = () => {
        const loginData = {
            email: this.get().email,
            password: this.get().password,
            captchaToken: this.get().captchaToken
        }

        return this.validate(loginData, signInValidator)
    }

    // firebase login
    firebaseLogin = async (props) => {
        const result = {};
        const { isGoogle } = props;
        let user;

        try {
            const provider = isGoogle ? googleProvider() : facebookProvider()
            const firebaseResponse = await signInWithPopup(auth, provider)
            const token = await auth.currentUser?.getIdToken(false);
            user = firebaseResponse.user;

            const axiosResponse = await axios({
                method: "POST",
                url: serverApi('/auth/login'),
                data: {
                    email: user.email,
                    [isGoogle ? "isGoogleAccount" : "isFacebookAccount"]: true,
                    token: token
                }

            })

            const responseData = axiosResponse.data
            result.token = token;
            if (responseData.message) {
                result.message = responseData.message
            }

            result.success = true;
        } catch (error) {
            if (user) {
                await deleteUser(user);
            }
            if (error instanceof FirebaseError) {
                result.noNotification = true;
            }
            result.error = error;
            result.message = error.message;
            if (error instanceof AxiosError) {
                // console.log('firebaseRegisterError',{error})
                result.error = error.response.data;
                result.message = error.response.data.message;
            }
        }

        return result
    }

    // resetPassword request
    resetPasswordRequest = async () => {
        const result = {};

        try {
            const axiosResponse = await axios({
                method: "POST",
                url: serverApi("/auth/reset-password"),
                params: {
                    email: this.get().email
                }
            })

            const responseData = axiosResponse.data
            if (responseData.message) {
                result.infoMessage = responseData.message
            }

            result.success = true;
        } catch (error) {
            result.error = error;
            result.message = error.message;

            if (error instanceof AxiosError) {
                // console.log('resetPasswordRequestError',{error})
                result.error = error.response.data;
                result.message = error.response.data.message;
            }
        }

        return result
    }

    // validate resetPasswordRequest Data
    validateResetPasswordRequestData = () => {
        return this.validate({ email: this.get().email }, emailValidator);
    }

    // verify email for register
    verifyEmailToken = async (props) => {
        const result = {};

        try {
            await axios({
                method: "PUT",
                url: serverApi(`/auth/verify-email/${props.token}`),
            })

            result.success = true
        } catch (error) {
            result.error = error;
            result.message = error.message;

            if (error instanceof AxiosError) {
                // console.log('verifyEmailTokenError',{error})
                result.error = error.response.data;
                result.message = error.response.data.message;
            }
        }
        result.noNotification = true;
        return result
    }

    // reset password
    resetPassword = async (props) => {
        const result = {};

        try {
            const axiosResponse = await axios({
                method: "PUT",
                url: serverApi("/auth/reset-password"),
                headers: {
                    'Authorization': props.token
                },
                data: {
                    password: this.get().password,
                    confirmPassword: this.get().confirmPassword,
                }
            })
            const responseData = axiosResponse.data
            if (responseData.message) {
                result.message = responseData.message
            }
            result.success = true;
        } catch (error) {
            result.error = error;
            result.message = error.message;
            if (error instanceof AxiosError) {
                // console.log('resetPassword Error',{error})
                result.error = error.response.data;
                result.message = error.response.data.message;
            }
        }

        return result
    }

    // validate resetPassword Data
    validateResetPasswordData = () => {
        const resetPasswordData = {
            password: this.get().password,
            confirmPassword: this.get().confirmPassword
        }
        return this.validate(resetPasswordData, resetPasswordValidator)
    }

    // signOut
    signOut = async () => {
        const result = {};
        const state = this.get();
        try {
            await axios({
                method: "DELETE",
                url: serverApi(`/auth/sign-out/${state.token}`)
            })

            const userState = useUserState.getState();
            userState.deleteEverything();
            state.deleteEverything();

            result.success = true;
        } catch (error) {
            result.error = error;
            result.message = error.message;
            if (error instanceof AxiosError) {
                // console.log('signOutError',{error})
                result.error = error.response.data;
                result.message = error.response.data.message;
            }
        }

        return result
    }
}
