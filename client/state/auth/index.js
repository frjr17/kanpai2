import { State } from "../../utils/functions/state"
import AuthStateFunctions from "./functions"

const initialState = {
    name: "",
    lastName: "",
    email: "",
    password: "",
    confirmPassword: "",
    token: "",
    captchaToken: "",
    captchaRef: undefined
}

export const useAuthState = new State('AuthState', initialState, AuthStateFunctions, {
    isPersist: true,
    persist: {
        partialize: (state) => ({ token: state.token })
    }
})