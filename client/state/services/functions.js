import axios, { AxiosError } from "axios";
import { serverApi } from "../../utils/functions";
import GlobalStateFunctions from "../../utils/functions/state";

export default class ServicesStateFunctions extends GlobalStateFunctions {
    constructor(initialState, store) {
        super(initialState, store);
    }



    getStatesAndCities = async () => {
        const result = {};

        try {
            // Fetching data from backend
            const axiosResponse = await axios({
                method: "GET",
                url: serverApi('/services/states-and-cities')
            })
            const responseData = axiosResponse.data;
            result.states = responseData.states
            result.message = responseData.message
            result.success = true;
            result.noNotification = true
        } catch (error) {
            // Adding error params to final obj...
            result.error = error;
            result.message = error.message;

            if (error instanceof AxiosError) {
                // console.error('getStatesAndCitiesError',error)
                result.error = error.response.data;
                result.message = error.response.data.message;
            }
        }

        return result;
    }

}