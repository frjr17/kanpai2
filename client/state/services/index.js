import { State } from "../../utils/functions/state"
import ServicesStateFunctions from "./functions"

const initialState = {
    states: undefined
}

export const useServicesState = new State('ServicesState', initialState, ServicesStateFunctions, { isPersist: true, })