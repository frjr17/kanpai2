import { State } from "../../utils/functions/state"
import UserStateFunctions from "./functions"

const initialState = {
    id: "",
    name: "",
    lastName: "",
    email: "",
    phoneNumber: "",
    profileImg: "",
    roles: undefined,
    state: "",
    city: "",
    createdAt: undefined,
    currentPassword: '',
    newPassword: "",
}

export const useUserState = new State('UserState', initialState, UserStateFunctions, { isPersist: true })