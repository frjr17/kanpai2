import axios, { AxiosError } from "axios";
import { serverApi } from "../../utils/functions";
import GlobalStateFunctions from "../../utils/functions/state";
import { passwordValidator, personalProfileValidator } from "./validators";


export default class UserStateFunctions extends GlobalStateFunctions {
    constructor(initialState, store) {
        super(initialState, store)
    }

    isAdmin = () => this.get().roles.find(role=>role.nameId === 'ADMIN')
    isProvider = () => this.get().roles.find(role=>role.nameId === 'PROVIDER')

    setUser = (user) => this.set({ ...user });

    fullName = () => `${this.get().name} ${this.get().lastName}`

    // getProfile
    getProfile = async (props) => {
        let result = {};
        try {
            const axiosResponse = await axios({
                method: "GET",
                url: serverApi('/user/profile'),
                headers: {
                    Authorization: props.token,
                }
            })
            const responseData = axiosResponse.data

            result = { ...result, ...responseData.user };

            if (responseData.message) {
                result.message = responseData.message
            }
            result.noNotification = true;
            result.success = true;
        } catch (error) {
            result.error = error;
            result.message = error.message;
            if (error instanceof AxiosError) {
                // console.log('getProfileError',{error})
                result.error = error.response.data;
                result.message = error.response.data.message;
            }
        }

        return result
    }

    // updateProfile
    updateProfile = async (props) => {
        const result = {};
        const state = this.get();
        try {
            const axiosResponse = await axios({
                method: "PUT",
                url: serverApi('/user/profile'),
                headers: {
                    Authorization: props.token,
                },
                data: {
                    name: state.name,
                    lastName: state.lastName,
                    phoneNumber: state.phoneNumber,
                    state: state.state,
                    city: state.city,
                }
            })
            const responseData = axiosResponse.data

            if (responseData.message) {
                result.message = responseData.message
            }
            result.success = true;
        } catch (error) {
            result.error = error;
            result.message = error.message;
            if (error instanceof AxiosError) {
                // console.log('updateProfileError',{error})
                result.error = error.response.data;
                result.message = error.response.data.message;
            }
        }

        return result
    }

    // validateUpdateProfile Data
    validateUpdateProfileData = () => {
        const state = this.get();
        const data = {
            name: state.name,
            lastName: state.lastName,
            phoneNumber: state.phoneNumber,
            state: state.state,
            city: state.city,
        }

        return this.validate(data, personalProfileValidator)
    }

    // updatePassword
    updatePassword = async (props) => {
        const result = {};
        const state = this.get();
        try {
            const axiosResponse = await axios({
                method: 'PUT',
                url: serverApi("/user/password"),
                headers: {
                    Authorization: props.token,
                },
                data: {
                    currentPassword: state.currentPassword,
                    newPassword: state.newPassword,
                }
            })
            const responseData = axiosResponse.data

            if (responseData.message) {
                result.message = responseData.message
            }
            result.success = true;
        } catch (error) {
            result.error = error;
            result.message = error.message;
            if (error instanceof AxiosError) {
                // console.log('updatePasswordError',{error})
                result.error = error.response.data;
                result.message = error.response.data.message;
            }
        }

        return result
    }

    // validateUpdatePassword Data
    validateUpdatePasswordData = () => {
        const passwordData = {
            currentPassword: this.get().currentPassword,
            newPassword: this.get().newPassword,
        };
        return this.validate(passwordData, passwordValidator)
    }
}   