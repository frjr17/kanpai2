import { lazy, object, string } from "yup";
import { passwordValidatorParams } from "../../utils/validators";


export const personalProfileValidator = object({
  name: string().required("Tienes que escribir tu nombre."),
  lastName: string().required("Tienes que escribir tu apellido"),
  phoneNumber: lazy((value) => (!value ? string().notRequired() : string().matches(/^[1-9][0-9]{8,10}$/, "El numero ingresado no corresponde a la ciudad de México."))),
  state: lazy((value) => (!value ? string().notRequired() : string().min(3, "Tienes que escribir 3 caracteres mínimo").max(60, "Solo puedes escribir hasta 60 caracteres"))),
  city: lazy((value) => (!value ? string().notRequired() : string().min(3, "Tienes que escribir 3 caracteres mínimo").max(60, "Solo puedes escribir hasta 60 caracteres"))),
});

export const passwordValidator = object({
  currentPassword: string().required("Tienes que escribir la constraseña actual."),
  newPassword: passwordValidatorParams,
});
