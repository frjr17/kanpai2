import axios, { AxiosError } from "axios";
import { businessForm, providerProfileForm } from "../../utils/forms/provider";
import { serverApi } from "../../utils/functions";
import GlobalStateFunctions from "../../utils/functions/state";

export default class ProviderStateFunctions extends GlobalStateFunctions {
    constructor(initialState, store) {
        super(initialState, store);
    }


    // ProviderProfile Form
    ProfileForm = (props) => {
        const state = this.get();
        let exclude = []
        if (!state.hasRfc) {
            exclude += ['rfcType', 'rfc']
        }
        providerProfileForm.inputs.state.options = props.states.map(state => state.name)

        if (state.state === 'Ciudad de México') {
            exclude += ['city']
            providerProfileForm.inputs.businessMayorsOffice.options = props.states.filter(({ name }) => name === state.state)[0].cities
        } else {
            exclude += ['businessMayorsOffice']

        }


        if (state.state) {
            providerProfileForm.inputs.city.options = props.states.filter(({ name }) => name === state.state)[0].cities

        } else {
            providerProfileForm.inputs.city.options = props.states[0].cities

        }

        return this.getFormInputs(providerProfileForm, [], exclude)

    }
    // Business Form
    BusinessProfileForm = () => {
        return this.getFormInputs(businessForm);
    }

    register = async (props) => {
        const result = {};
        const state = this.get();

        try {
            // Fetching data from backend
            const axiosResponse = await axios({
                method: "POST",
                url: serverApi('/provider/register'),
                headers: {
                    Authorization: props.token
                },
                data: {
                    hasRfc: state.hasRfc,
                    rfcType: state.rfcType,
                    rfc: state.rfc,
                    curp: state.curp,
                    state: state.state,
                    city: state.city,
                    email: state.delegateEmail,
                    termsAndConditions: state.termsAndConditions,
                    postalCode: state.postalCode,
                    proofOfAddreessImg: state.proofOfAddreessImg,
                    mayorsOffice: state.businessMayorsOffice,
                    business: {
                        name: state.businessName,
                        email: state.businessEmail,
                        phone: state.businessPhone,
                        line: state.businessLine,
                    },
                    delegate: {
                        name: state.delegateName,
                        phone: state.delegatePhone,
                        documentImage: state.delegateDocumentImage
                    }
                }
            })
            const responseData = axiosResponse.data;

            result.infoMessage = responseData.message
            result.success = true;
        } catch (error) {
            // Adding error params to final obj...
            result.error = error;
            result.message = error.message;

            if (error instanceof AxiosError) {
                // console.error('registerError',error)
                result.error = error.response.data;
                result.message = error.response.data.message;
            }
        }

        return result;
    }

    verify = async (props) => {
        const result = {};

        try {
            // Fetching data from backend
            const axiosResponse = await axios({
                method: "POST",
                url: serverApi("/provider/verify"),
                data: {
                    providerId: props.providerId,
                    isAccepted: props.isAccepted
                },
                headers: {
                    Authorization: props.token
                }
            })
            const responseData = axiosResponse.data;

            result.message = responseData.message
            result.success = true;
        } catch (error) {
            // Adding error params to final obj...
            result.error = error;
            result.message = error.message;

            if (error instanceof AxiosError) {
                // console.error('verifyError',error)
                result.error = error.response.data;
                result.message = error.response.data.message;
            }
        }

        return result;
    }

}