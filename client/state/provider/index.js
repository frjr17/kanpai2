import { State } from "../../utils/functions/state"
import ProviderStateFunctions from "./functions"

const initialState = {
    id: '',
    hasRfc: true,
    rfcType: "Física",
    rfc: "",
    curp: "",
    city: "",
    state: "",
    termsAndConditions: false,
    proofOfAddreessImg: "",
    postalCode: "",
    businessName: "",
    businessEmail: "",
    businessPhone: "",
    businessMayorsOffice: "",
    businessLogoImg: "",
    businessLine: "",
    delegateName: "",
    delegatePhone: "",
    delegateDocumentImage: "",
    delegateEmail: "",
}

export const useProviderState = new State('ProviderState', initialState, ProviderStateFunctions)