import { Box, HStack, Text, VStack, Icon, Heading, UnorderedList, ListItem, Wrap, WrapItem } from "@chakra-ui/react";
import Image from "next/image";
import { BiBell } from "react-icons/bi";
import Carousel from "../../../components/carousel";
import { bannerImages } from "../../../utils/constants/pages";

/**Component that renders service publication page service data. */
export default function ServiceData() {
  /**React slick settings for carousel */
  const settings = {
    appendDots: function (dots) {
      return (
        <Wrap sx={styles.carouselWrapContainer} style={{ position: "static" }} justify="center">
          {dots.map((dot, i) => (
            <WrapItem key={i} style={{ width: 125, height: 75, margin: 10 }} display={"block"}>
              {dot.props.children}
            </WrapItem>
          ))}
        </Wrap>
      );
    },
    customPaging: function (i) {
      return <Image alt={"thumb"} width={125} height={75} src={`/images/banner/home-${i + 1}.jpg`} style={{ display: "block", height: 75, objectFit: "cover" }} />;
    },
    dots: true,
    dotsClass: "slick-dots slick-thumb",
  };
  return (
    <VStack sx={styles.container}>
      {/* Carousel */}
      <Box sx={styles.carouselContainer}>
        <Carousel height={{ base: 500, md: "60vh" }} carouselHeight={500} cards={bannerImages} settings={settings} />
      </Box>
      {/* Tags */}
      <VStack alignItems={" start"} padding={5}>
        <HStack>
          <HStack>
            <Icon as={BiBell} />
            <Text sx={styles.tags}>Mexicana</Text>
          </HStack>
          <HStack>
            <Icon as={BiBell} />
            <Text sx={styles.tags}>Cocina Tradicional</Text>
          </HStack>
        </HStack>
        {/* Description and caracteristics */}
        <Box>
          <Heading as={"h3"} sx={styles.heading}>
            Descripcion
          </Heading>
          <Text>¡Acompaña tu evento con nuestros lonches de pierna!</Text>
        </Box>
        <Box>
          <Heading as={"h3"} sx={styles.heading}>
            Características
          </Heading>
          <UnorderedList>
            <ListItem>Lonches</ListItem>
            <ListItem>Verduras</ListItem>
            <ListItem>Bebidas</ListItem>
            <ListItem>Desechables</ListItem>
          </UnorderedList>
        </Box>
        {/* Restrictions  */}
        <Box>
          <Heading as={"h3"} sx={styles.heading}>
            Restricciones
          </Heading>
          <Text>Meseros</Text>
        </Box>
        {/* Cancellation policies */}
        <Box>
          <Heading as={"h3"} sx={styles.heading}>
            Politicas de cancelación
          </Heading>
          <UnorderedList>
            <ListItem>No devoluciones en el anticipo</ListItem>
            <ListItem>Se aparta fecha con el 50% de descuento</ListItem>
          </UnorderedList>
        </Box>
      </VStack>
    </VStack>
  );
}

const styles = {
  container: {
    textAlign: "left",
    alignItems: "start",
    width: "100%",
  },
  carouselContainer: {
    width: { base: "90vw", md: "100%" },
  },
  tags: {
    fontSize: "lg",
  },
  heading: {
    fontSize: "xl",
    marginBottom: 3,
  },
  carouselWrapContainer: {
    display: { base: "none", md: "flex" },
    height: "100%",
    overflow: "initial",
    position: "static",
    spacing: 5,
  },
};
