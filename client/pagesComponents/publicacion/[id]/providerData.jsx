import { Avatar, Box, Button, Heading, HStack, Text, VStack, Wrap, WrapItem } from "@chakra-ui/react";
import { useState } from "react";
import DatePicker from "react-datepicker";
import NumberInput from "../../../components/numberInput";

/**Component that renders service post provider data. */
export default function ProviderData() {
  // Using state for react datepicker (something I have to change for a zustand state)
  const [date, setDate] = useState(new Date());

  // Function that changes calendar date.
  const onChange = (date) => {
    setDate(date);
  };
  return (
    <VStack align={{ base: "center", md: "start" }} spacing={5}>
      {/* Avatar */}
      <Avatar marginX={"auto"} size={"xl"} src={"/images/banner/home-3.jpg"} />
      {/* Service Name */}
      <Heading sx={styles.title} as={"h1"}>
        Lonches Karlos
      </Heading>
      <Box>
        <Heading as={"h2"} sx={styles.heading}>
          Zonas de Servicio
        </Heading>
      </Box>
      <Box>
        {/* Price per day */}
        <Heading as={"h2"} sx={styles.heading}>
          Precio (por persona)
        </Heading>
        <Wrap sx={styles.weekPricesContainer}>
          <WrapItem sx={styles.weekPriceContainer}>
            <Box>
              <Heading as={"h3"} fontSize={"md"}>
                Domingo
              </Heading>
              <Text>150.00 MXN</Text>
            </Box>
          </WrapItem>
          <WrapItem sx={styles.weekPriceContainer}>
            <Box>
              <Heading as={"h3"} fontSize={"md"}>
                Lunes
              </Heading>
              <Text>150 MXN</Text>
            </Box>
          </WrapItem>
          <WrapItem sx={styles.weekPriceContainer}>
            <Box>
              <Heading as={"h3"} fontSize={"md"}>
                Martes
              </Heading>
              <Text>150 MXN</Text>
            </Box>
          </WrapItem>
          <WrapItem sx={styles.weekPriceContainer}>
            <Box>
              <Heading as={"h3"} fontSize={"md"}>
                Miercoles
              </Heading>
              <Text>150 MXN</Text>
            </Box>
          </WrapItem>
          <WrapItem sx={styles.weekPriceContainer}>
            <Box>
              <Heading as={"h3"} fontSize={"md"}>
                Jueves
              </Heading>
              <Text>150 MXN</Text>
            </Box>
          </WrapItem>
          <WrapItem sx={styles.weekPriceContainer}>
            <Box>
              <Heading as={"h3"} fontSize={"md"}>
                Viernes
              </Heading>
              <Text>150 MXN</Text>
            </Box>
          </WrapItem>
          <WrapItem sx={styles.weekPriceContainer}>
            <Box>
              <Heading as={"h3"} fontSize={"md"}>
                Sábado
              </Heading>
              <Text>150 MXN</Text>
            </Box>
          </WrapItem>
        </Wrap>
      </Box>
      <Box>
        {/* Aditional costs */}
        <Heading as={"h2"} sx={styles.heading}>
          Costo adicional
        </Heading>
        <HStack>
          Outzone cost
          <Heading as={"h3"} fontSize={"md"}>
            Costo Fuera de Zona:
          </Heading>
          <Text>Si</Text>
        </HStack>
      </Box>
      <VStack align={{ base: "center", md: "start" }} spacing={1}>
        {/* Important details section */}
        <Heading as={"h2"} sx={styles.heading}>
          Restricciones
        </Heading>
        <HStack>
          <Heading as={"h3"} fontSize={"md"}>
            Renta Mínima:
          </Heading>
          <Text>20 personas</Text>
        </HStack>
        <HStack>
          <Heading as={"h3"} fontSize={"md"}>
            Renta máxima:
          </Heading>
          <Text>200 personas</Text>
        </HStack>
        <HStack>
          <Heading as={"h3"} fontSize={"md"}>
            Requiere anticipo:
          </Heading>
          <Text>Si, 50% del costo</Text>
        </HStack>
      </VStack>
      <Box>
        {/* Cotization date with datepicker */}
        <Heading as={"h2"} sx={styles.heading}>
          Cotización
        </Heading>
        <Box>
          <DatePicker selected={date} onChange={onChange} excludeDates={[new Date()]} inline />
        </Box>
      </Box>
      <HStack>
        {/* People Quantity using custom number input. */}
        <Heading as={"h3"} fontSize={"md"}>
          Cantidad de Personas
        </Heading>
        <NumberInput />
      </HStack>
      {/* Sum Part */}
      <HStack>
        <Text fontSize={"2xl"}>Anticipo:</Text>
        <Heading as={"h2"} sx={styles.heading}>
          0.00 MXN
        </Heading>
      </HStack>
      <HStack>
        <Text fontSize={"2xl"}>Total:</Text>
        <Heading as={"h2"} sx={styles.heading}>
          0.00 MXN
        </Heading>
      </HStack>
      {/* Form Part */}
      <Button>Agregar Cotización</Button>
    </VStack>
  );
}

const styles = {
  title: {
    textAlign: "center",
    width: "full",
  },
  heading: {
    fontSize: "2xl",
    textAlign: { base: "center", md: "left" },
    marginBottom: 3,
  },
  weekPricesContainer: {
    justifyContent: "center",
    width: "full",
    textAlign: "center",
  },
  weekPriceContainer: {
    width: "40%",
    justifyContent: "center",
  },
};
