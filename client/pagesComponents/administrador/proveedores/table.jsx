import { Button, ButtonGroup, Table, TableContainer, Tbody, Td, Th, Thead, Tr } from "@chakra-ui/react";
import { useAdminState } from "../../../state/admin";
import { useProviderState } from "../../../state/provider";

export default function ProvidersTable(props) {
    const { acceptProvider, rejectProvider } = props
    const adminState = useAdminState();
    const providerState = useProviderState();

    return <TableContainer background={"white"}>
        <Table size={{ base: "sm", md: "md" }}>
            <Thead background={"blackAlpha.800"} >
                <Tr>
                    <Th color={"white"} textAlign={"center"}>Proveedor</Th>
                    <Th display={{ base: "none", md: "table-cell" }} color={"white"} textAlign={"center"}>Titular</Th>
                    <Th display={{ base: "none", md: "table-cell" }} color={"white"} textAlign={"center"}>Email</Th>
                    <Th display={{ base: "none", md: "table-cell" }} color={"white"} textAlign={"center"}>Teléfono</Th>
                    <Th display={{ base: "none", md: "table-cell" }} color={"white"} textAlign={"center"}>Estado</Th>
                    <Th display={{ base: "none", md: "table-cell" }} color={"white"} textAlign={"center"}>Ciudad</Th>
                    <Th color={"white"} textAlign={"center"}>Acción/Estado</Th>
                </Tr>
            </Thead>
            <Tbody>
                {
                    adminState.providers.map((provider) => {
                        const accept = () => acceptProvider(provider.id);
                        const reject = () => rejectProvider(provider.id);
                        return <Tr key={provider.rfc}>
                            <Td>{provider.business.name}</Td>
                            <Td display={{ base: "none", md: "table-cell" }}>{provider.delegate.name}</Td>
                            <Td display={{ base: "none", md: "table-cell" }}>{provider.business.email}</Td>
                            <Td display={{ base: "none", md: "table-cell" }}>+52 {provider.business.phone}</Td>
                            <Td display={{ base: "none", md: "table-cell" }}>{provider.state}</Td>
                            <Td display={{ base: "none", md: "table-cell" }}>{provider.city}</Td>
                            <Td>
                                {
                                    provider.isActive ? 'ACTIVO' : <ButtonGroup>
                                        <Button isLoading={providerState.isFetching || adminState.isFetching} loadingText={""} onClick={accept} size={{ base: 'sm', md: "md" }} variant={'solid'} colorScheme={"whatsapp"} marginRight={5}>Aceptar</Button>
                                        <Button isLoading={providerState.isFetching || adminState.isFetching} loadingText={""} onClick={reject} size={{ base: 'sm', md: "md" }} variant={'solid'} colorScheme={"red"}>Rechazar</Button>
                                    </ButtonGroup>
                                }
                            </Td>
                        </Tr>
                    })
                }
            </Tbody>

        </Table>
    </TableContainer>
}