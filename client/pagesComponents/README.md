<img src="./../public/images/logo/logo%20-%20s.png" alt="drawing" width="100"/>

# **_Kanpai Project - PagesComponents_** 🔥

When all page code stays in one page, it gets so cluttered and complicated to handle, that's where pagesComponents comes into place. Pages Components folder gets in charge of splitting page components into manageable and reusable files, simplifyng the pages folder without compromising [Next.js routing](https://nextjs.org/docs/routing/introduction).

All components in this folder are stored inside its own carpet, with the components perfectly named. If there's some *index.js* folder, it's because the component file is available four multiple component pages as the main page for CSR purposes. 