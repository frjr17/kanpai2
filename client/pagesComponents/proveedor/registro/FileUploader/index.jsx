import { Button, Center, Divider, Heading, HStack, Input, Text, VStack } from "@chakra-ui/react";

export default function FileUploader(props) {
    const { dropzone, title } = props;

    return (<Center width={{ base: "100%", md: '40%' }}>
        <VStack {...styles.fileUploader}>
            <Heading fontSize={"xl"}>{title}</Heading>
            <Divider />
            <Text>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptatem, nihil hic? Eligendi quas cum ad omnis natus, dolorum excepturi at vel nobis saepe quae expedita molestiae debitis provident quia quaerat?
            </Text>
            <HStack width={"100%"} height={'40px'} {...dropzone.getRootProps()} spacing={0}>
                <Center {...styles.fileUploaderText}>
                    {dropzone.isDragActive ? "Suelta el archivo aquí!" : dropzone.acceptedFiles.length ? dropzone.acceptedFiles[0].name : 'Haz click o arrastra los archivos aqui!'}
                </Center>
                <Input {...dropzone.getInputProps()} borderTopRightRadius={0} borderBottomRightRadius={0} />
                <Button borderTopLeftRadius={0} borderBottomLeftRadius={0}>{dropzone.acceptedFiles.length ? 'Subir' : 'Buscar'}</Button>
            </HStack>
        </VStack>
    </Center>)
}

const styles = {
    fileUploadContainer: {
        width: "100%",
        flexWrap: "wrap",
        justifyContent: "space-around"
    },
    fileUploader: {
        spacing: 5,
        marginY: 10,
        border: '2px solid',
        borderColor: "gray.500",
        padding: 10,
        width: 500,
    },
    fileUploaderText: {
        padding: "10px",
        overflow: "hidden",
        textOverflow: "ellipsis",
        width: "100%",
        height: "100%",
        borderRadius: "md",
        border: "1px solid",
        borderColor: "gray.500",
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0
    }
}