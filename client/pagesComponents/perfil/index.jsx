import { Avatar, Heading, HStack, Text, VStack, Wrap, WrapItem, Icon, Button } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { AiFillStar } from "react-icons/ai";
import { useAuthState } from "../../state/auth";
import { useUserState } from "../../state/user";
import { personalForm, securityForm } from "../../utils/forms/profile";
import FormInputs from "./formInputs";
import { NotificationManager } from "react-notifications";
import LoadingPage from "../../components/loadingPage";
import _ from "lodash";
import { useProfileState } from "../../utils/hooks";

export default function ProfilePage() {
  const userState = useUserState();
  const authState = useAuthState();
  const { token } = authState;
  const isAdmin = Boolean(userState.roles?.find((role) => role.nameId !== "USER"));
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(true);

  const personalStateData = {
    name: userState.name,
    lastName: userState.lastName,
    phoneNumber: userState.phoneNumber,
    state: userState.state,
    city: userState.city,
  };

  const { personalState, passwordState, changeState } = useProfileState(userState);
  let createdAt;
  if (userState.createdAt) {
    createdAt = new Date(userState.createdAt);
  }

  useEffect(() => {
    getUserProfile();

    return () => {
      userState.setUser({
        ...personalState,
        currentPassword: "",
        newPassword: "",
      });
    };

    async function getUserProfile() {
      if (!token) {
        NotificationManager.info("Para ingresar a tu perfil debes iniciar sesión primero");
      } else {
        const result = await userState.getProfile({ token });
        changeState(result);
        setIsLoading(false);
      }
    }
  }, [token]);

  if (!authState.token) {
    router.push("/iniciar-sesion");
  }

  if (isLoading) {
    return <LoadingPage />;
  }

  const updateUserProfile = async (event) => {
    event.preventDefault();
    try {
      userState.deleteFormErrors();

      await userState.validateUpdateProfileData();
      await userState.updateProfile({ token });
      changeState(personalStateData);
    } catch (error) { }
  };

  const updateUserPassword = async (event) => {
    event.preventDefault();
    try {
      userState.deleteFormErrors();

      await userState.validateUpdatePasswordData();
      await userState.updatePassword({ token });

      const emptyPasswordFields = { currentPassword: "", newPassword: "" };
      userState.setUser(emptyPasswordFields);
      changeState(emptyPasswordFields, false);
    } catch (error) { }
  };
  return (
    <Wrap width={{ base: "95%", md: "80%" }} margin={"auto"} paddingY={10} justify={"space-between"}>
      <WrapItem padding={10} alignItems={"center"} background={"white"} width={"100%"} height={200}>
        <Avatar size={"2xl"} src={userState.profileImg || ""} name={`${personalState.name} ${personalState.lastName}`} />
        <VStack marginLeft={5} align={"start"}>
          <HStack>
            <Heading as={"h1"} fontSize={"2xl"}>
              {personalState.name} {personalState.lastName}
            </Heading>
            {isAdmin && (
              <HStack background={"blue"} rounded={"md"} paddingY={"3px"} paddingX={"10px"}>
                <Icon as={AiFillStar} w={3} h={3} color={"white"} />
                <Text fontSize={"md"} fontStyle={"italic"} color={"white"}>
                  Administrador
                </Text>
              </HStack>
            )}
          </HStack>
          <Text fontSize={"md"} fontStyle={"italic"} color={"gray.500"}>
            Miembro desde {createdAt && createdAt.toLocaleDateString()}
          </Text>
        </VStack>
      </WrapItem>
      <WrapItem as={"form"} onSubmit={updateUserProfile} padding={7} flexDirection={"column"} width={{ base: "100%", md: "48%" }} background={"white"}>
        <HStack justify={"space-between"} width={"100%"} marginBottom={5}>
          <Heading as={"h3"} fontSize={"2xl"}>
            Informacion Personal
          </Heading>
          <Button isDisabled={_.isEqual(personalState, personalStateData)} isLoading={userState.isFetching} loadingText={"..."} type="submit">
            GUARDAR
          </Button>
        </HStack>
        <VStack width={"100%"} spacing={5}>
          <FormInputs form={personalForm} />
        </VStack>
      </WrapItem>
      <WrapItem as={"form"} onSubmit={updateUserPassword} padding={7} flexDirection={"column"} width={{ base: "100%", md: "48%" }} background={"white"}>
        <HStack justify={"space-between"} width={"100%"} marginBottom={5}>
          <Heading as={"h3"} fontSize={"2xl"}>
            Seguridad
          </Heading>
          <Button isDisabled={userState.currentPassword === passwordState.currentPassword} type={"submit"} loadingText={"..."} isLoading={userState.isFetching}>
            GUARDAR
          </Button>
        </HStack>
        <VStack width={"100%"} spacing={5}>
          <FormInputs form={securityForm} />
        </VStack>
      </WrapItem>
    </Wrap>
  );
}
