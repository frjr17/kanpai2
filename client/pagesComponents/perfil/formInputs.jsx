import CustomInput from "../../components/customInput";
import { useUserState } from "../../state/user";

export default function FormInputs(props) {
  const userState = useUserState();

  return (
    <>
      {props.form.map((formInput) => {
        formInput.value = userState[formInput.name] || "";
        formInput.onChange = userState.handleChange(formInput.name);

        formInput.isDisabled = userState.isFetching;

        return <CustomInput key={formInput.name} {...formInput} errors={userState.formErrors ? userState.formErrors[formInput.name] : null} />;
      })}
    </>
  );
}
