import { Button, FormControl, FormLabel, Heading, Input, Select, Stack } from "@chakra-ui/react";

/**Page that  */
export default function CTA() {
  return (
    <Stack as={"form"} sx={styles.container} spacing={10} align={"center"}>
      <Heading sx={styles.heading}>Te ayudamos a buscar entre cientos de proveedores para organizar el evento de tus sueños</Heading>
      <Stack direction={["column", "row"]} spacing={10} width={"80%"}>
        {/* Form */}
        <FormControl>
          <FormLabel>¿Qué servicio estás buscando?</FormLabel>
          <Select background={"white"} placeholder="Selecciona de la lista"></Select>
        </FormControl>
        <FormControl>
          <FormLabel>¿Donde será tu evento?</FormLabel>
          <Input background={"white"} placeholder="Escribe o Selecciona" />
        </FormControl>
      </Stack>
      {/* Call to Action. */}
      <Button type={"submit"}>BUSCAR</Button>
    </Stack>
  );
}

const styles = {
  container: {
    direction: "column",
    padding: 5,
  },
  heading: {
    textAlign: "center",
    fontSize: { base: "xl", md: "3xl" },
    width: { base: "90%", md: "75%" },
  },
};
