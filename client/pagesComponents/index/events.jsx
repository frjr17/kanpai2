import { Heading, VStack, Wrap, WrapItem, Icon } from "@chakra-ui/react";
import { events } from "../../utils/constants/pages";

/**Compoent that renders homepage events. */
export default function Events() {
  return (
    <VStack sx={styles.container} spacing={10}>
      <Heading sx={styles.heading}>Organiza cualquier tipo de evento, en tan solo unos clicks</Heading>
      <Wrap justify={"center"}>
        {/* Component events card. */}
        {events.map((event) => (
          <WrapItem key={event.label} sx={styles.wrapItem}>
            <Icon as={event.icon} w={20} h={20} color={"white"} />
            <Heading as={"h5"} sx={styles.label}>
              {event.label}
            </Heading>
          </WrapItem>
        ))}
      </Wrap>
    </VStack>
  );
}

const styles = {
  container: {
    padding: 10,
  },
  heading: {
    textAlign: "center",
    fontSize: "3xl",
  },
  wrapItem: {
    rounded: "md",
    justifyContent: "space-around",
    alignItems: "center",
    textAlign: "center",
    padding: 4,
    width: 150,
    height: 200,
    flexDirection: "column",
    bg: "anzac.400",
  },
  label: {
    fontSize: "xl",
    color: "white",
  },
};
