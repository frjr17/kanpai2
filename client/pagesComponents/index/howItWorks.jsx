import { Card, CardBody, Icon, Heading, Stack, Text, VStack, Wrap, WrapItem } from "@chakra-ui/react";
import { howItWorks } from "../../utils/constants/pages";

/**Component that renders homepage howItWorks section. */
export default function HowItWorks() {
  return (
    <VStack spacing={10} padding={10}>
      <Heading fontSize={"3xl"}>¿Cómo funciona?</Heading>
      <Wrap justify={"center"} spacing={10}>
        {/* How it works card  */}
        {howItWorks.map((step) => (
          <WrapItem key={step.label}>
            <Card sx={styles.card} align={"center"}>
              <CardBody textAlign={"center"}>
                <Icon as={step.icon} w={20} h={20} color={"anzac.400"} />
                <Stack textAlign={"center"}>
                  <Heading as={"h4"} fontSize={"2xl"}>
                    {step.label}
                  </Heading>
                  <Text fontSize={"md"}>{step.description}</Text>
                </Stack>
              </CardBody>
            </Card>
          </WrapItem>
        ))}
      </Wrap>
    </VStack>
  );
}

const styles = {
  container: {
    padding: 10,
  },
  card: {
    background: "white",
    width: 250,
    height: 250,
  },
};
