import { Avatar, Box, Button, Card, CardBody, CardFooter, CardHeader, Flex, Heading, Stack, Text, WrapItem } from "@chakra-ui/react";
import Image from "next/image";
import NextLink from "next/link";
import { dummyAvatarImage, dummyBannerImage } from "../../utils/constants/dummies";

/**Component that renders panel services card. */
export default function CardComponent() {
  return (
    <WrapItem>
      <Card sx={styles.container}>
        <CardHeader padding={0}>
          <Box sx={styles.imageContainer}>
            <Image alt={"Photo"} height={100} width={425} style={{ objectFit: "cover" }} src={dummyBannerImage} />
          </Box>
        </CardHeader>
        <Flex sx={styles.avatarContainer}>
          <Avatar size={"xl"} src={dummyAvatarImage} border={"2px solid white"} />
        </Flex>
        <CardBody display={"flex"} flexDirection={"column"} justifyContent={"space-between"} paddingX={"15px"} paddingY={"5px"}>
          <Stack spacing={0}>
            <Heading sx={styles.heading}>LONCHES KARLOS</Heading>
            <Text color={"gray.500"}>Jalisco, Guadalajara</Text>
          </Stack>
          {/* Service Detail. */}
          <Text>Lo mejor para tu evento</Text>
          <Stack direction={"row"} justify={"space-between"} spacing={6}>
            <Text>3,000.00 MXN</Text>
            <Text>p/Persona</Text>
          </Stack>
        </CardBody>
        <CardFooter paddingX={"15px"} paddingBottom={"15px"} paddingTop={"5px"}>
          <Button as={NextLink} href={"/publicacion/1"} sx={styles.button}>
            COTIZAR
          </Button>
        </CardFooter>
      </Card>
    </WrapItem>
  );
}

const styles = {
  container: {
    width: 325,
    minHeight: 500,
    overflow: "hidden",
    background: "white",
  },
  imageContainer: {
    height: 250,
    width: "100%",
    overflow: "hidden",
  },
  avatarContainer: {
    justifyContent: "center",
    marginTop: -12,
  },

  heading: {
    fontSize: "2xl",
    fontWeight: 500,
  },
  button: {
    width: "full",
    color: "white",
    rounded: "md",
  },
};
