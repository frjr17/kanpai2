import Image from "next/image";

/**Component that displays Kanpai Logo.*/
export default function KanpaiLogo(props) {
  return <Image priority style={{ width: props.width, height: props.height }} alt={"Kanpai Logo"} width={props.width} height={props.height} src={"/svg/logo.svg"} />;
}
