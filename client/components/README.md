<img src="./../public/images/logo/logo%20-%20s.png" alt="drawing" width="100"/>

# **_Kanpai Project - Components_** 🔥

This folder component contains all reusable components necessary for app pages.

## *Caracteristics*
* The component is often used in two or more pages.
* The component is often used in two or more layouts
* Each component has its own folder (with splitted components if necessary).

## **Components Available**
* [AuthNavField](authNavField/index.jsx): Decides, if it's some user registered, to display the user menu or the sign buttons.
* [carousel](carousel/index.jsx): Displays an image slider using [React Slick](https://react-slick.neostack.com/) dependancy 
* [customInput](customInput/index.jsx): Handler for every component input. 
* [kanpaiLogo](kanpaiLogo/index.jsx): Displays Kanpai Logo svg
* [loadingPage](loadingPage/index.jsx): Displays the loading page.
* [loadingSpinner](loadingSpinner/index.jsx): Displays a loading spinner
* [numberInput](numberInput/index.jsx): Handler for number inputs.
