import { Center, Spinner } from "@chakra-ui/react";
import SignButtons from "./signButtons";
import UserNav from "./userNav";
import { useAuthState } from "../../state/auth";
/**Component that connects with auth state to toggle between sign buttons and user display header.*/
export default function AuthNavField() {
  const authState = useAuthState();

  // If auth state is fetching (example= it's signing out) it shows spinner
  // else, toggles between user navigation options and sign buttons.
  if (authState.isFetching) {
    return (
      <Center minWidth={{ base: "50px", md: "185px" }}>
        <Spinner size={{ base: "sm", md: "md" }} color={"anzac.400"} />
      </Center>
    );
  } else {
    return authState.token ? <UserNav /> : <SignButtons />;
  }
}
