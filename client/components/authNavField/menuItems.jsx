import { Icon, MenuItem } from "@chakra-ui/react";
import NextLink from "next/link";
import { useUserState } from "../../state/user";
import { adminMenuItems, menuItems, providerMenuItems } from "../../utils/constants/components";

export default function MenuItems() {
  const userState = useUserState();

  let items = menuItems
 
  if (userState.roles) {
    if (userState.isProvider()) {
      items = providerMenuItems
    }

    if (userState.isAdmin()) {
      items = adminMenuItems
    }
  }

  return (
    <>
      {items.map((item) => {
        return (
          <MenuItem icon={<Icon as={item.Icon} color={"anzac.400"} w={4} h={4} />} key={item.href} as={NextLink} href={item.href}>
            {item.label}
          </MenuItem>
        );
      })}
    </>
  );
}
