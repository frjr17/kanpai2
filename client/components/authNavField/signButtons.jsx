import { Button, ButtonGroup, Link } from "@chakra-ui/react";
import NextLink from "next/link";

/**Component that displays sign in and sign up buttons. */
export default function SignButtons() {
  return (
    <ButtonGroup justifyContent={"center"}>
      <Link as={NextLink} sx={styles.signInLink} href={"/iniciar-sesion"}>
        Iniciar Sesión
      </Link>
      <Button sx={styles.signUpButton} as={NextLink} href={"/registro"}>
        Regístrate
      </Button>
    </ButtonGroup>
  );
}

const styles = {
  signInLink: {
    margin: 0,
    padding: 2,
    fontSize: "sm",
    fontWeight: 500,
  },
  signUpButton: {
    display: { base: "none", md: "inline-flex" },
    fontSize: "sm",
    fontWeight: 600,
    color: "white",
  },
};
