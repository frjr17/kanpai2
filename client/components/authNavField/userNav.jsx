import { Avatar, Box, HStack, Menu, MenuButton, MenuDivider, MenuItem, MenuList, VStack } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { FiChevronDown } from "react-icons/fi";
import { useAuthState } from "../../state/auth";
import { useUserState } from "../../state/user";
import MenuItems from "./menuItems";

/**Component that displays user navigation with options*/
export default function UserNav() {
  // Calling states
  const user = useUserState();
  const auth = useAuthState();

  // Calling router to use redirect function
  const router = useRouter();

  /**Sign out current user and redirects to main page ("/")*/
  const signOut = async () => {
    try {
      // Signing out
      await auth.signOut();
      // Getting root page after signing out
      router.push("/");
    } catch (error) { }
  };
  return (
    <Menu>
      <MenuButton py={2} transition="all 0.3s" _focus={{ boxShadow: "none" }}>
        <HStack>
          <Avatar size={"sm"} name={user.fullName()} src={user.profileImg || ""} />
          <VStack sx={styles.justNotMobile} alignItems="flex-start" spacing={0} ml="2">
            {/* <Text fontSize="sm">{user.fullName()}</Text>
            <Text fontSize={"2xs"} color="gray.600">
              {user.roles?.map((role) => role.name).join(", ")}
            </Text> */}
          </VStack>
          <Box sx={styles.justNotMobile}>
            <FiChevronDown />
          </Box>
        </HStack>
      </MenuButton>
      <MenuList bg={"white"} borderColor={"white"}>
        <MenuItems />
        <MenuDivider />
        <MenuItem onClick={signOut}>Cerrar Sesión</MenuItem>
      </MenuList>
    </Menu>
  );
}

const styles = {
  justNotMobile: {
    display: { base: "none", md: "flex" },
  },
};
