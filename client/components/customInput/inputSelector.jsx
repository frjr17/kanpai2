import { Input } from "@chakra-ui/react";
import BooleanSelector from "./BooleanSelector";
import PasswordSelector from "./PasswordSelector";
import RadioInputs from "./RadioInputs";
import Recaptcha from "./recaptcha";
import SelectInput from "./SelectInput";

export default function InputSelector(props) {
  const { inputProps, errors } = props;
  const redColor = errors ? "red" : "inherit";

  switch (inputProps.type) {

    case 'select':
      return <SelectInput inputProps={inputProps} />

    case "radio":
      return <RadioInputs inputProps={inputProps} />

    case "boolean":
      return <BooleanSelector inputProps={inputProps} />

    case "captcha":
      return <Recaptcha onChange={inputProps.onChange} />;

    case "password":
      return <PasswordSelector inputProps={inputProps} errors={errors} />

    default:
      return <Input {...inputProps} borderColor={redColor} color={redColor} />
  }
}
