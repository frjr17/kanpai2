import { FormControl, FormErrorMessage, FormHelperText, FormLabel, InputGroup, InputLeftAddon, InputRightAddon } from "@chakra-ui/react";
import InputSelector from "./inputSelector";

/**Component that renders chakra input.
 * The difference between this and the base chakra input is that it displays a button to show password in case the input.type === "password"
 */
export default function CustomInput(props) {
  const { errors, styles, label, leftAddonChildren, rightAddonChildren, helpers, ...inputProps } = props;

  return (
    <FormControl sx={styles} isRequired={inputProps.isRequired} isInvalid={Boolean(props.errors && props.errors.length)}>
      <FormLabel>{label}</FormLabel>
      <InputGroup>
        {Boolean(leftAddonChildren) && <InputLeftAddon>{leftAddonChildren}</InputLeftAddon>}
        <InputSelector errors={errors} inputProps={inputProps} />
        {Boolean(rightAddonChildren) && <InputRightAddon>{rightAddonChildren}</InputRightAddon>}
      </InputGroup>

      {Boolean(helpers) && helpers?.map((helper) => <FormHelperText key={helper}>{helper}</FormHelperText>)}

      {Boolean(errors?.length) && errors?.map((error) => <FormErrorMessage key={error}>{error}</FormErrorMessage>)}
    </FormControl>
  );
}
