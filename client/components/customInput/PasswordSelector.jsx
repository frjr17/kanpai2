import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import { Button, Input, InputRightElement } from "@chakra-ui/react";
import { useState } from "react";


export default function PasswordSelector(props) {
    const [showPassword, setShowPassword] = useState(false);
    const { inputProps, errors } = props;
    const redColor = errors ? "red" : "inherit";
    return (<>
        <Input {...inputProps} borderColor={redColor} color={redColor} type={showPassword ? "text" : "password"} />
        <InputRightElement h={"full"}>
            <Button variant={"ghost"} onClick={() => setShowPassword((showPassword) => !showPassword)}>
                {showPassword ? <ViewIcon /> : <ViewOffIcon />}
            </Button>
        </InputRightElement>
    </>)
}