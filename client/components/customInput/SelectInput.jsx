import { Select } from '@chakra-ui/react'

export default function SelectInput(props) {
    const { options, ...inputProps } = props.inputProps;

    return <Select {...inputProps}>
        {options.map(option => <option name={option} key={option}>{option}</option>)}
    </Select>

}