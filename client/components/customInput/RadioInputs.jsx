import { Radio, RadioGroup, Stack } from "@chakra-ui/react";



export default function RadioInputs(props) {
    const { inputProps } = props;



    return <RadioGroup as={Stack} direction={'column'} align={"center"} spacing={4} onChange={(value) => { inputProps.onChange({ target: { value } }) }} value={inputProps.value}>

        {inputProps.radioInputs.map(input => {
            return <Radio key={input} value={input}>{input}</Radio>
        })}
    </RadioGroup>
}