import { Radio, RadioGroup, Stack } from "@chakra-ui/react"

export default function BooleanSelector(props) {
    const { inputProps } = props;

    const onChange = (value) => {
        if (value === 'Si') {
            inputProps.onChange({ target: { value: true } })
        } else {
            inputProps.onChange({ target: { value: false } })
        }
        return
    }

    return <RadioGroup as={Stack} direction={'column'} align={"center"} spacing={4} onChange={onChange} value={inputProps.value ? "Si" : "No"}>
        <Radio value={'Si'}>Si</Radio>
        <Radio value={'No'}>No</Radio>
    </RadioGroup>
}