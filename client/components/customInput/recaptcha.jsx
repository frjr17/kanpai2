import { Box } from "@chakra-ui/react";
import { createRef } from "react";
import ReCAPTCHA from "react-google-recaptcha";

export const captchaRef = createRef();
export default function Recaptcha(props) {
  const { onChange } = props;

  return (
    <Box margin={"auto"}>
      <ReCAPTCHA
        ref={captchaRef}
        sitekey={process.env.NEXT_PUBLIC_RECAPTCHA_PUBLIC_KEY}
        onChange={(token) => {
          if (token) {
            const tokenToHandle = { target: { value: token } };
            if (onChange) {
              onChange(tokenToHandle);
            }
          }
        }}
      />
    </Box>
  );
}
