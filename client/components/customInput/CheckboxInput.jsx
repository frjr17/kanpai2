import { Checkbox } from "@chakra-ui/react";

export default function CheckboxInput(props) {
    const { inputProps } = props;

    const onChange = (event) => {
        const value = event.target.checked

        inputProps.onChange({ target: { value } })
    }

    return <Checkbox {...inputProps} onChange={onChange}>
        {props.label}
    </Checkbox>
}