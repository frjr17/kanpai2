import { Center, Spinner } from "@chakra-ui/react";

export default function LoadingSpinner(props) {
  return (
    <Center minWidth={{ base: "50px", md: "185px" }} {...props.sx}>
      <Spinner size={{ base: "xs", md: "md" }} color={"anzac.400"} />
    </Center>
  );
}
