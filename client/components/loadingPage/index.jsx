import { Box, Center, Heading, Spinner } from "@chakra-ui/react";
import KanpaiLogo from "../kanpaiLogo";

/**Component that displays a loading page */
export default function LoadingPage(props) {
  return (
    <Center sx={styles.container}>
      <Box sx={styles.logoContainer}>
        <KanpaiLogo width={200} height={200} />
      </Box>
      <Spinner size="xl" color="anzac.400" />
      {props.children ? <Heading sx={styles.heading}>{props.children}</Heading> : null}
    </Center>
  );
}

const styles = {
  container: {
    height: "100vh",
    flexDirection: "column",
  },
  logoContainer: {
    marginBottom: 10,
  },
  heading: {
    marginTop: 10,
  },
};
