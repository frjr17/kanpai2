import { AddIcon, MinusIcon } from "@chakra-ui/icons";
import { Input, InputGroup, InputLeftElement, InputRightElement, useNumberInput } from "@chakra-ui/react";

/**Component that displays NumberInput between minus and add buttons.*/
export default function NumberInput(props) {
  // Using useNumberInput hook
  const { getInputProps, getIncrementButtonProps, getDecrementButtonProps } = useNumberInput({
    defaultValue: 1,
    min: 0,
    max: 50,
  });

  return (
    <InputGroup width={props.width || 150}>
      <InputLeftElement {...getIncrementButtonProps()} sx={styles.inputLeftElement}>
        <AddIcon />
      </InputLeftElement>
      {/* Input statement */}
      <Input _focusVisible={{ border: "1px solid gray.300", boxShadow: "none" }} {...getInputProps()} textAlign={"center"} fontWeight={600} />
      <InputRightElement {...getDecrementButtonProps()} sx={styles.inputRightElement}>
        <MinusIcon />
      </InputRightElement>
    </InputGroup>
  );
}

const styles = {
  inputLeftElement: {
    background: "anzac.400",
    color: "white",
    rounded: "md",
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
  },

  inputRightElement: {
    background: "anzac.400",
    color: "white",
    rounded: "md",
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
  },
};
