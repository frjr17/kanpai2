import { Button, Text, VStack } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import LoadingPage from "../../client/components/loadingPage";
import SignLayout from "../../client/layouts/signLayout";
import { useAuthState } from "../../client/state/auth";
import NextLink from "next/link";

export default function ActivateAccount() {
  // Loading state set to true to start web page refreshing...
  const [isLoading, setIsLoading] = useState(true);

  // Extracting neccessary props from authorization state
  const { verifyEmailToken, error } = useAuthState();

  // Calling router and extracting token from it.
  const router = useRouter();
  const token = router.query.token;

  useEffect(() => {
    // If there's token, activate account
    if (token) {
      activateAccount();
    }

    async function activateAccount() {
      try {
        await verifyEmailToken({ token });
      } catch (error) { }
      setIsLoading(false);
    }
  }, [token]);

  // Displaying loading page if it's "isLoading" prop is set to true.
  if (isLoading) {
    return <LoadingPage>Verificando Token</LoadingPage>;
  }

  return (
    <SignLayout title={"Activar Cuenta"}>
      <VStack>
        <Text fontSize={"lg"} fontStyle={"italic"}>
          {error ? error.message : "Cuenta Verificada! Ya puedes iniciar sesion"}
        </Text>
        {!error && (
          <Button as={NextLink} href={"/iniciar-sesion"}>
            Iniciar Sesion
          </Button>
        )}
      </VStack>
    </SignLayout>
  );
}
