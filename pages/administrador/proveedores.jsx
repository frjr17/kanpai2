import { Heading, VStack, Text } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import LoadingSpinner from "../../client/components/loadingSpinner";
import MainLayout from "../../client/layouts/mainLayout";
import ProvidersTable from "../../client/pagesComponents/administrador/proveedores/table";
import { useAdminState } from "../../client/state/admin";
import { useAuthState } from "../../client/state/auth";
import { useProviderState } from "../../client/state/provider";


export default function Providers() {
    // Calling States...
    const adminState = useAdminState();
    const authState = useAuthState();
    const providerState = useProviderState()

    // Using isLoading state to display spinner first on table when fetching.
    const [isLoading, setIsLoading] = useState(true);

    // Extracting providers from admin.
    const { providers } = adminState

    useEffect(() => {
        getProviders();
        async function getProviders() {
            try {
                await adminState.getProviders({ token: authState.token });
            } catch (error) { }
            setIsLoading(false)
        }
    }, [])

    /**Function that accept providers */
    const acceptProvider = async (id) => {
        try {
            await providerState.verify({ providerId: id, isAccepted: true, token: authState.token })
            await adminState.getProviders({ token: authState.token });
        } catch (error) { }
    }

    /**Function that rejects and deletes providers */
    const rejectProvider = async (id) => {
        try {
            await providerState.verify({ providerId: id, isAccepted: false, token: authState.token })
            await adminState.getProviders({ token: authState.token });
        } catch (error) { }
    }

    return <MainLayout title={"Proveedores"}>
        <VStack padding={{ base: 5, md: 10 }} spacing={5}>
            <Heading>Proveedores</Heading>
            {isLoading ? <LoadingSpinner /> : !providers.length ? <Text fontStyle={"italic"} fontSize={"2xl"}>
                No hay Proveedores por el momento.
            </Text> : <ProvidersTable acceptProvider={acceptProvider} rejectProvider={rejectProvider} />}
        </VStack>
    </MainLayout>
}