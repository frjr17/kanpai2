import { Box, ChakraProvider } from "@chakra-ui/react";
import { NotificationContainer } from "react-notifications";
import 'react-notifications/lib/notifications.css';
import "react-datepicker/dist/react-datepicker.css";
import { mainTheme } from "../client/styles/theme";

export default function App({ Component, pageProps }) {
  return <ChakraProvider theme={mainTheme}>
    <Box>
      <Component {...pageProps} />
      <NotificationContainer />
    </Box>
  </ChakraProvider>
}
