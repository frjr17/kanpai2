import dynamic from "next/dynamic";
import LoadingPage from "../../client/components/loadingPage";
import MainLayout from "../../client/layouts/mainLayout";
const ProfilePage = dynamic(() => import("../../client/pagesComponents/perfil"), { ssr: false, loading: () => <LoadingPage /> });
/**Page that renders user profile. */
export default function Profile() {
  return (
    <MainLayout>
      <ProfilePage />
    </MainLayout>
  );
}
