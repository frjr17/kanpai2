import { Wrap, WrapItem } from "@chakra-ui/react";
import MainLayout from "../../client/layouts/mainLayout";
import ProviderData from "../../client/pagesComponents/publicacion/[id]/providerData";
import ServiceData from "../../client/pagesComponents/publicacion/[id]/serviceData";

/**Page that renders a provider service as a post. */
export default function Post() {
  return (
    <MainLayout>
      <Wrap justify={"space-around"} sx={styles.container}>
        {/* Displaying service data */}
        <WrapItem sx={styles.serviceDataContainer}>
          <ServiceData />
        </WrapItem>
        {/* Displaying provider data. */}
        <WrapItem sx={styles.providerDataContainer}>
          <ProviderData />
        </WrapItem>
      </Wrap>
    </MainLayout>
  );
}

const styles = {
  container: {
    width: "full",
    padding: 5,
  },
  serviceDataContainer: {
    width: { base: "full", md: "100%" },
    maxWidth: { base: "full", md: "60%" },
    minWidth: { base: "full", md: 600 },
    background: "white",
  },
  providerDataContainer: {
    justifyContent: "space-around",
    padding: 5,
    flexDirection: "column",
    width: { base: "full", md: "30%" },
    background: "white",
    minWidth: { base: "full", md: 275 },
  },
};
