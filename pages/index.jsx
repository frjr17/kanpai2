import { bannerImages } from "../client/utils/constants/pages";
import CTA from "../client/pagesComponents/index/CTA";
import HowItWorks from "../client/pagesComponents/index/howItWorks";
import Events from "../client/pagesComponents/index/events";
import MainLayout from "../client/layouts/mainLayout";
import Carousel from "../client/components/carousel";

/**Homepage Component */
export default function HomePage() {
  return (
    <MainLayout>
      {/*======== Image Carousel ========*/}
      <Carousel cards={bannerImages} height={"50vh"} settings={{ dots: false }} />
      {/*======== CTA ========*/}
      <CTA />
      {/*======== Section >> How it Works! ========*/}
      <HowItWorks />
      {/*======== Section >> Events ========*/}
      <Events />
    </MainLayout>
  );
}
