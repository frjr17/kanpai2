import { Button, Center, Divider, Heading, Link, Text, VStack } from "@chakra-ui/react";
import { useCallback, useEffect, useState } from "react";
import CheckboxInput from "../../../client/components/customInput/CheckboxInput";
import LoadingPage from "../../../client/components/loadingPage";
import MainLayout from "../../../client/layouts/mainLayout";
import { useProviderState } from "../../../client/state/provider";
import { useServicesState } from "../../../client/state/services";
import NextLink from 'next/link'
import { useDropzone } from "react-dropzone";
import axios from "axios";
import { useRouter } from "next/router";
import { useAuthState } from "../../../client/state/auth";
import FileUploader from "../../../client/pagesComponents/proveedor/registro/FileUploader";

export default function ProviderRegister() {
    const providerState = useProviderState();
    const servicesState = useServicesState()
    const [isLoading, setIsLoading] = useState(true)
    const router = useRouter();

    const onIdentityImgDrop = useCallback(async (files) => {
        try {
            const { data } = await axios.post("/api/provider/upload-temp-file", {
                name: files[0].name,
                type: files[0].type
            })

            const url = data.url;

            await axios.put(url, files[0], {
                'Content-Type': files[0].type,
                "Access-Control-Allow-Origign": '*'
            })
            providerState.handleChange("delegateDocumentImage")({ target: { value: url } })
        } catch (error) { }
    }, []);

    const onProofOfAddressDrop = useCallback(async (files) => {
        try {
            const { data } = await axios.post("/api/provider/upload-temp-file", {
                name: files[0].name,
                type: files[0].type
            })

            const url = data.url;
            await axios.put(url, files[0], {
                'Content-Type': files[0].type,
                "Access-Control-Allow-Origign": '*'
            })
            providerState.handleChange("proofOfAddreessImg")({ target: { value: url } })
        } catch (error) { }
    }, []);

    const identityImgDropzone = useDropzone({ maxFiles: 1, onDrop: onIdentityImgDrop });
    const proofOfAddressDropzone = useDropzone({ maxFiles: 1, onDrop: onProofOfAddressDrop });

    const { token } = useAuthState();
    const handleSubmit = async (event) => {
        event.preventDefault()
        try {
            await providerState.register({ token });
            providerState.deleteEverything();

            router.push("/panel")
        } catch (error) { }
    }

    useEffect(() => {
        getStatesAndCities();

        async function getStatesAndCities() {
            try {
                await servicesState.getStatesAndCities();
                setIsLoading(false);
                if (!providerState.state) {
                    if (servicesState.states) {
                        providerState.handleChange("state")({ target: { value: servicesState.states[0].name } })
                    }
                }
                if (!providerState.city) {
                    if (servicesState.states) {
                        providerState.handleChange("city")({ target: { value: servicesState.states[0].cities[0] } })
                    }
                }
            } catch (error) { }
        }
    }, [])

    if (isLoading) {
        return <LoadingPage />
    }

    const termsAndConditionsLabel = <Text>Al dar click aquí, acepto el Aviso de Privacidad y  los <Link as={NextLink} color={"blue.400"} href={"/proveedor/terminos"}>Terminos y Condiciones</Link></Text>;
    const termsAndConditionsProps = { onChange: providerState.handleChange("termsAndConditions"), value: providerState.termsAndConditions, name: 'termsAndConditions' };
    return <MainLayout title={"Registro de Proveedores"}>
        <VStack as={"form"} onSubmit={handleSubmit} padding={{ base: 5, md: 10 }}>
            <Heading {...styles.heading}>Formulario de Proveedores</Heading>
            <VStack {...styles.formContainer}>
                <Center {...styles.form}>
                    <Heading {...styles.formHeading}>1. Datos del Proveedor</Heading>
                    <Divider />
                    <providerState.ProfileForm states={servicesState.states} />
                </Center>
                <Center {...styles.form}>
                    <Heading {...styles.formHeading}>2. Datos del Negocio</Heading>
                    <Divider />
                    <providerState.BusinessProfileForm />
                </Center>
                <Heading {...styles.formHeading}>3. Documentos de Identidad</Heading>
                <Divider />
                <Center {...styles.fileUploadContainer}>
                    <FileUploader dropzone={identityImgDropzone} title={'Adjunte su documento de Identidad'} />
                    <FileUploader dropzone={proofOfAddressDropzone} title={'Adjunte su comprobante de domicilio'} />
                </Center>

                <CheckboxInput inputProps={termsAndConditionsProps} label={termsAndConditionsLabel} />

                <Button isLoading={providerState.isFetching} loadingText={""} type={"submit"}>Registrate como Proveedor</Button>
            </VStack>
        </VStack>
    </MainLayout>
}

const styles = {
    heading: {
        textAlign: "center",
        marginBottom: { base: 5, md: 10 },
    },
    formContainer: {
        background: 'white',
        width: { base: "full", md: "90%" },
        padding: 5
    },
    form: {
        flexWrap: 'wrap',
        justifyContent: "start"
    },
    formHeading: {
        as: 'h3',
        fontSize: "2xl",
        width: "100%",
        marginY: 5
    },

}