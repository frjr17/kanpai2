import { Center, Divider, Heading, Text } from "@chakra-ui/react";
import CustomInput from "../../client/components/customInput";
import { useProviderState } from "../../client/state/provider";



export default function TermsAndConditions() {
    const providerState = useProviderState();
    return (<Center flexWrap={'wrap'} width={'100%'} justifyContent={'start'}>
        <Heading as={"h3"} fontSize={"2xl"} width={"100%"} marginY={5}>Términos y Condiciones</Heading>
        <Divider />

        <Text my={5}>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis quidem quo vel pariatur amet asperiores distinctio eligendi molestias at sequi quis repellendus, illo est ipsum ducimus ex impedit enim id.
            Excepturi dolorum laudantium ea, fugit perspiciatis voluptatum impedit quo itaque error, odit autem, doloremque deleniti? Repellat similique consectetur nulla, incidunt adipisci nostrum quis cupiditate quae? Modi a officiis quod reprehenderit.
            Atque laborum, perferendis ducimus earum harum saepe nulla vero excepturi maxime quo tempora! Animi ea assumenda esse delectus laboriosam libero commodi repellendus porro doloremque, ad doloribus in a perspiciatis aperiam.</Text>
        <Text my={5}>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis quidem quo vel pariatur amet asperiores distinctio eligendi molestias at sequi quis repellendus, illo est ipsum ducimus ex impedit enim id.
            Excepturi dolorum laudantium ea, fugit perspiciatis voluptatum impedit quo itaque error, odit autem, doloremque deleniti? Repellat similique consectetur nulla, incidunt adipisci nostrum quis cupiditate quae? Modi a officiis quod reprehenderit.
            Atque laborum, perferendis ducimus earum harum saepe nulla vero excepturi maxime quo tempora! Animi ea assumenda esse delectus laboriosam libero commodi repellendus porro doloremque, ad doloribus in a perspiciatis aperiam.</Text>
        <Text my={5}>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis quidem quo vel pariatur amet asperiores distinctio eligendi molestias at sequi quis repellendus, illo est ipsum ducimus ex impedit enim id.
            Excepturi dolorum laudantium ea, fugit perspiciatis voluptatum impedit quo itaque error, odit autem, doloremque deleniti? Repellat similique consectetur nulla, incidunt adipisci nostrum quis cupiditate quae? Modi a officiis quod reprehenderit.
            Atque laborum, perferendis ducimus earum harum saepe nulla vero excepturi maxime quo tempora! Animi ea assumenda esse delectus laboriosam libero commodi repellendus porro doloremque, ad doloribus in a perspiciatis aperiam.</Text>
        <Text my={5}>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis quidem quo vel pariatur amet asperiores distinctio eligendi molestias at sequi quis repellendus, illo est ipsum ducimus ex impedit enim id.
            Excepturi dolorum laudantium ea, fugit perspiciatis voluptatum impedit quo itaque error, odit autem, doloremque deleniti? Repellat similique consectetur nulla, incidunt adipisci nostrum quis cupiditate quae? Modi a officiis quod reprehenderit.
            Atque laborum, perferendis ducimus earum harum saepe nulla vero excepturi maxime quo tempora! Animi ea assumenda esse delectus laboriosam libero commodi repellendus porro doloremque, ad doloribus in a perspiciatis aperiam.</Text>
        <Divider my={3} />
        <CustomInput value={providerState} onChange={providerState.handleChange("termsAndConditions")} type={'boolean'} label={"Acepto los términos y condiciones"} isRequired={true} />
    </Center>)
}