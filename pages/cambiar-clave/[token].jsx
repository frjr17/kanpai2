import { Button, Text, VStack } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import LoadingPage from "../../client/components/loadingPage";
import SignLayout from "../../client/layouts/signLayout";
import { useAuthState } from "../../client/state/auth";

export default function ResetPassword() {
  // Calling authorization state
  const authState = useAuthState();

  // These states are implemented to handle this page messages and loadings...
  const [isLoading, setIsLoading] = useState(true);
  const [message, setMessage] = useState("Verificando Token");
  const [error, setError] = useState(authState.error);

  // Calling useRouter to extract page token.
  const router = useRouter();
  const token = router.query.token;

  useEffect(() => {
    // Verifying toke when it loads.
    if (token) {
      verifyToken();
    }

    async function verifyToken() {
      try {
        await authState.verifyResetPasswordToken({ token });
      } catch (error) {
        setError(error);
      }
      setIsLoading(false);
    }

    // Deleting everything from state when leaving the page.
    return () => {
      authState.deleteEverything();
    };
  }, [token]);

  const handleSubmit = async (event) => {
    event.preventDefault();

    // Changing local state for loading
    setMessage("Cambiando Contraseña");
    setIsLoading(true);
    setError(undefined);

    try {
      // Validating user data before reseting password
      await authState.validateResetPasswordData();
      // Resetting password
      await authState.resetPassword({ token });
      // Deleting everything from db if everything is successful and pushing to login page.
      authState.deleteEverything();
      router.push("/iniciar-sesion");
      return;
    } catch (error) { }
    // Turning loading off..
    setIsLoading(false);
  };

  // Displaying loading page if "isLoading" prop is true.
  if (isLoading) {
    return <LoadingPage>{message}</LoadingPage>;
  }

  return (
    <SignLayout title={"Cambio de Contraseña"}>
      <VStack width={"full"} as={"form"} onSubmit={handleSubmit}>
        {error ? (
          <Text fontSize={"lg"} fontStyle={"italic"}>
            {error.message}
          </Text>
        ) : (
          <>
            <authState.ResetPasswordForm />
            <Button isLoading={authState.isFetching} loadingText={"Cambiando Contraseña"} type={"submit"}>
              Cambiar contraseña
            </Button>
          </>
        )}
      </VStack>
    </SignLayout>
  );
}
