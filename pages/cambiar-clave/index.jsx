import { Button, VStack } from "@chakra-ui/react";
import { useEffect } from "react";
import SignLayout from "../../client/layouts/signLayout";
import { useAuthState } from "../../client/state/auth";

export default function ResetPasswordRequest() {
  // Calling authorization state
  const authState = useAuthState();

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      // Validating data before sending resetPassword request...
      await authState.validateResetPasswordRequestData();
      await authState.resetPasswordRequest();
      // Deleting everything from state...
      authState.deleteEverything();
    } catch (error) { }
  };
  useEffect(() => {
    return () => {
      // Deleting everything when leaving the page
      authState.deleteEverything("token");
    };
  }, []);


  return (
    <SignLayout title={"Cambio de Contraseña"}>
      <VStack as={"form"} onSubmit={handleSubmit} width={"full"}>
        {/* I'm using signIn form first index (email data) to populate custom input props. */}
        <authState.ResetPasswordRequestForm />
        <Button type={"submit"} isLoading={authState.isFetching} loadingText={"Solicitando Cambio"}>
          Solicitar Cambio
        </Button>
      </VStack>
    </SignLayout>
  );
}
