import { createRouter } from 'next-connect'
import AuthController from '../../../server/controllers/auth';
import GlobalController from '../../../server/controllers/global';
import UserController from '../../../server/controllers/user';
import UserValidators from '../../../server/controllers/user/validators';

const router = createRouter();

// getProfile
router.get(AuthController.authorize(), UserController.getProfile, GlobalController.sendState);

// updateProfile
router.put(AuthController.authorize(), UserValidators.updateProfile(), GlobalController.checkValidators, UserController.updateProfile, GlobalController.sendState)


export default router.handler({
    onError: GlobalController.errorController,
    onNoMatch: GlobalController.notFound
})