import { createRouter } from 'next-connect'
import AuthController from '../../../server/controllers/auth';
import GlobalController from '../../../server/controllers/global';
import UserController from '../../../server/controllers/user';
import UserValidators from '../../../server/controllers/user/validators';

const router = createRouter();

// updatePassword 
router.put(AuthController.authorize(), UserValidators.updatePassword(), GlobalController.checkValidators, UserController.updatePassword, GlobalController.sendState)


export default router.handler({
    onError: GlobalController.errorController,
    onNoMatch: GlobalController.notFound
})