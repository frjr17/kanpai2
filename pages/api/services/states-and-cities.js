import { createRouter } from 'next-connect'
import GlobalController from '../../../server/controllers/global';
import ServicesController from '../../../server/controllers/services';

const router = createRouter();

router.get(ServicesController.getCitiesAndStates, GlobalController.sendState)

export default router.handler({
    onError: GlobalController.errorController,
    onNoMatch: GlobalController.notFound
})