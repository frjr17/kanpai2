import { createRouter } from 'next-connect'
import AuthController from '../../../server/controllers/auth';
import GlobalController from '../../../server/controllers/global';
import AuthValidators from '../../../server/controllers/auth/validators';

const router = createRouter();

// login
router.post(...AuthValidators.login(), GlobalController.checkValidators, AuthController.login, GlobalController.sendState)



export default router.handler({
    onError: GlobalController.errorController,
    onNoMatch: GlobalController.notFound
})