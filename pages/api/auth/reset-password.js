import { createRouter } from 'next-connect'
import AuthController from '../../../server/controllers/auth';
import GlobalController from '../../../server/controllers/global';
import AuthValidators from '../../../server/controllers/auth/validators';

const router = createRouter();

// resetPasswordRequest
router.post(...AuthValidators.resetPasswordRequest(), GlobalController.checkValidators, AuthController.resetPasswordRequest, GlobalController.sendEmail, GlobalController.sendState)
// resetPassword 
router.put(...AuthValidators.resetPassword(), GlobalController.checkValidators, AuthController.resetPassword, GlobalController.sendState)

export default router.handler({
    onError: GlobalController.errorController,
    onNoMatch: GlobalController.notFound
})