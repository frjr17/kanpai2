import { createRouter } from 'next-connect'
import AuthController from '../../../server/controllers/auth';
import GlobalController from '../../../server/controllers/global';
import AuthValidators from '../../../server/controllers/auth/validators';

const router = createRouter();

// register
router.post(...AuthValidators.register(), GlobalController.checkValidators, AuthController.register, GlobalController.sendEmail, GlobalController.sendState);

export default router.handler({
    onError: GlobalController.errorController,
    onNoMatch: GlobalController.notFound
})