import { createRouter } from 'next-connect'
import AuthController from '../../../../server/controllers/auth';
import GlobalController from '../../../../server/controllers/global';

const router = createRouter();

router.get(AuthController.verifyResetPasswordToken, GlobalController.sendState)

export default router.handler({
    onError: GlobalController.errorController,
    onNoMatch: GlobalController.notFound
})