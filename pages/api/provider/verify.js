import { createRouter } from 'next-connect'
import AuthController from '../../../server/controllers/auth';
import GlobalController from '../../../server/controllers/global';
import ProviderController from '../../../server/controllers/provider';

const router = createRouter();

// Verify Provider
router.post(AuthController.authorize('ADMIN'), ProviderController.verifyProvider, GlobalController.sendState);


export default router.handler({
    onError: GlobalController.errorController,
    onNoMatch: GlobalController.notFound
})