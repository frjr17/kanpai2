import { createRouter } from 'next-connect'
import AuthController from '../../../server/controllers/auth';
import GlobalController from '../../../server/controllers/global';
import ProviderController from '../../../server/controllers/provider';

const router = createRouter();

// Register Provider
router.get(AuthController.authorize('ADMIN'), ProviderController.getAll, GlobalController.sendState);

export default router.handler({
    onError: GlobalController.errorController,
    onNoMatch: GlobalController.notFound
})