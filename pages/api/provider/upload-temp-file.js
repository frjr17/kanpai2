import { createRouter } from 'next-connect'
import AuthController from '../../../server/controllers/auth';
import GlobalController from '../../../server/controllers/global';
import ProviderController from '../../../server/controllers/provider';

const router = createRouter();

// Register Provider
router.post(AuthController.authorize(), ProviderController.uploadTempFile, GlobalController.sendState);

export default router.handler({
    onError: GlobalController.errorController,
    onNoMatch: GlobalController.notFound
})