<img src="./../public/images/logo/logo%20-%20s.png" alt="drawing" width="100"/>

# **_Kanpai Project - Pages_** 🔥

This folder is required by Next.js to handle all pages routing. Here, are pages are displayed for client-side.

## **Pages Available**

Route => File
1. / => *index.jsx*
2. /iniciar-sesion => *iniciar-sesion.jsx*
3. /registro => *registro.jsx*
4. /activar-cuenta/[token] => *./activar-cuenta/[token].jsx*
5. /cambiar-clave => *./cambiar-clave/index.jsx*
6. /cambiar-clave/[token] => *./cambiar-clave/[token].jsx*
7. /panel => *./panel/index.jsx*
8. /perfil => *./perfil/index.jsx*
9. /publicacion/[id] => *./publicacion/[id].jsx*


