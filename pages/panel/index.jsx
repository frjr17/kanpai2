import { Heading, Wrap } from "@chakra-ui/react";
import PanelLayout from "../../client/layouts/panelLayout";
import Card from "../../client/pagesComponents/panel/card";

/**Page that renders the main panel. */
export default function Panel() {
  return (
    <PanelLayout>
      <Heading sx={styles.heading}>Comida</Heading>
      {/* Cards section. */}
      <Wrap justify={{ base: "center", md: "flex-start" }} sx={styles.cardContainer} spacing={5}>
        <Card />
      </Wrap>
    </PanelLayout>
  );
}

const styles = {
  heading: {
    textAlign: { base: "center", md: "left" },
  },
  cardContainer: {
    paddingY: 6,
  },
};
