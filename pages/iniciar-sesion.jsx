import { Button, ButtonGroup, Text, VStack, Icon, Link } from "@chakra-ui/react";
import { AiFillFacebook } from "react-icons/ai";
import { FcGoogle } from "react-icons/fc";
import NextLink from "next/link";
import SignLayout from "../client/layouts/signLayout";
import { useAuthState } from "../client/state/auth";
import { useEffect } from "react";
import { useRouter } from "next/router";
import { captchaRef } from "../client/components/customInput/recaptcha";

export default function SignIn() {
  // Calling authorization state
  const authState = useAuthState();
  // Calling router
  const router = useRouter();


  useEffect(() => {
    // Setting captchaRef
    authState.setCaptchaRef(captchaRef);
    return () => {
      // Deleting everything when leaving the page
      authState.deleteEverything("token");
    };
  }, []);


  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      // Validating data before logging in
      await authState.validateLoginData();
      // Logging in
      await authState.login();
      // Redirecting to panel if it was successful
      router.push("/panel");
    } catch (error) {
      authState.resetCaptcha();
    }
  };

  const firebaseLogin = async (isGoogle = true) => {
    try {
      await authState.firebaseLogin({ isGoogle });
      await router.push("/panel");
    } catch (error) { }
  };

  return (
    <SignLayout title={"Iniciar Sesión"}>
      <VStack as={"form"} onSubmit={handleSubmit} spacing={5} width={"100%"}>
        <authState.LoginForm />
        <Button isLoading={authState.isFetching} loadingText={"..."} type={"submit"} width={"full"}>
          Iniciar Sesion
        </Button>
        <Link as={NextLink} href={"/cambiar-clave"} color={"blue.400"}>
          ¿Olvidaste tu Contraseña?
        </Link>
      </VStack>
      <VStack>
        <ButtonGroup isDisabled={authState.isFetching}>
          <Button onClick={() => firebaseLogin()} background={"white"} borderColor={"anzac.200"} width={10} height={10} padding={5}>
            <Icon as={FcGoogle} boxSize={6} />
          </Button>
          <Button onClick={() => firebaseLogin(false)} background={"white"} borderColor={"anzac.200"} width={10} height={10} padding={5}>
            <Icon as={AiFillFacebook} boxSize={6} color={"blue.700"} />
          </Button>
        </ButtonGroup>
        <Text align={"center"}>
          ¿No tienes Cuenta?{" "}
          <Link as={NextLink} href={"/registro"} color={"blue.400"}>
            {"Registrate"}
          </Link>
        </Text>
      </VStack>
    </SignLayout>
  );
}
