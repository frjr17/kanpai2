import { Button, ButtonGroup, Text, VStack, Icon, Link } from "@chakra-ui/react";
import { AiFillFacebook } from "react-icons/ai";
import { FcGoogle } from "react-icons/fc";
import NextLink from "next/link";
import SignLayout from "../client/layouts/signLayout";
import { useAuthState } from "../client/state/auth";
import { useEffect } from "react";
import { captchaRef } from "../client/components/customInput/recaptcha";
import { useRouter } from "next/router";

export default function SignIn() {
  // Calling authorization state
  const authState = useAuthState();

  // Calling router hook
  const router = useRouter();

  useEffect(() => {
    // Setting captchaRef
    authState.setCaptchaRef(captchaRef);
    return () => {
      // Deleting everything when leaving the page
      authState.deleteEverything("token");
    };
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      // Validating data before registering
      await authState.validateRegisterData();
      // Registering...
      await authState.register();
      authState.deleteEverything();
    } catch (error) {
      authState.resetCaptcha();
    }
  };

  const firebaseRegister = async (isGoogle = true) => {
    try {
      await authState.firebaseRegister({ isGoogle });
      await router.push("/panel");
    } catch (error) { }
  };

  return (
    <SignLayout title={"Regístrate"}>
      <VStack as={"form"} onSubmit={handleSubmit} spacing={5} width={"100%"}>
        {/* FormInputs */}
        <authState.RegisterForm />
        <Button isLoading={authState.isFetching} loadingText={"..."} type="submit" width={"full"}>
          Regístrate
        </Button>
      </VStack>
      <VStack>
        <ButtonGroup isDisabled={authState.isFetching}>
          <Button onClick={() => firebaseRegister(true)} background={"white"} borderColor={"anzac.200"} width={10} height={10} padding={5}>
            <Icon as={FcGoogle} boxSize={6} />
          </Button>
          <Button onClick={() => firebaseRegister(false)} background={"white"} borderColor={"anzac.200"} width={10} height={10} padding={5}>
            <Icon as={AiFillFacebook} boxSize={6} color={"blue.700"} />
          </Button>
        </ButtonGroup>
        <Text align={"center"}>
          ¿Ya tienes Cuenta?{" "}
          <Link as={NextLink} href={"/iniciar-sesion"} color={"blue.400"}>
            {"Iniciar Sesión"}
          </Link>
        </Text>
      </VStack>
    </SignLayout>
  );
}
