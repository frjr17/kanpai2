import { Center, Heading, Text } from "@chakra-ui/react";
import Image from "next/image";
import MainLayout from "../client/layouts/mainLayout";

export default function NotFound() {

    return <MainLayout title={"Not Found"}>
        <Center height={"75vh"} flexDirection={'column'}>
            <Image width={200} height={200} src={"/images/logo/logo - s.png"} alt={'Kanpai Logo'} />
            <Heading fontSize={"5xl"} mt={5}>Lo Sentimos</Heading>
            <Text fontStyle={'italic'} fontSize={"2xl"}>La pagina que estas buscando no existe.</Text>
        </Center>
    </MainLayout>
}