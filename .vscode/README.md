<img src="./../public/images/logo/logo%20-%20s.png" alt="drawing" width="100"/>

# **_Kanpai Project - Snippets_** 🔥

This folder contains all snippets involved to create state functions, pages, components and everything else to agilize coding experience.

## State Snippets

### **1. stateCreate**

This snippet generates a fresh new State instance, after you type the command, it should look like this

```
import { State } from "../../utils/functions/state"
import {SomeStateName}StateFunctions from "./functions"

const initialState = {
    // write your state data structure here...
}

export const use{SomeStateName}State = new State('{SomeStateName}State', initialState, StateFunctions)
```

### **2. stateFunctionClassCreate**

This snippet generates a State function class. After you type the command, it should look like this.

```
import GlobalStateFunctions from "../../utils/functions/state";

export default class {SomeFunctionClassName}StateFunctions extends GlobalStateFunctions {
    constructor(initialState, store) {
        super(initialState, store);
    }

    
}
```


### **3. stateAsyncFunction**

This snippet generats an state async function inside the stateFunction class. After you type the command,it should look like this:

```
    {SomeFunctionName} = async () => {
        const result = {};
        const state = this.get();

        try {
            // Fetching data from backend
            const axiosResponse = await axios({})
            const responseData = axiosResponse.data;

            result.message = responseData.message
            result.sucess = true;
        } catch (error) {
            // Adding error params to final obj...
            result.error = error;
            result.message = error.message;

            if (error instanceof AxiosError) {
                // console.error('{SomeFunctionName}Error',error)
                result.error = error.response.data;
                result.message = error.response.data.message;
            }
        }

        return result;
    }
```