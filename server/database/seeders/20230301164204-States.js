'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up () {
    
    const statesAndCities = require("./data/statesAndCities");
    const State = require('../models').State;
    const City = require('../models').City;
    
    for (let stateAndCity of statesAndCities){
      let {state,cities } = stateAndCity
      
      state = await State.create({name:state})
      cities = cities.map(city=>({name:city,stateId:state.id}));
      await City.bulkCreate(cities); 
    }

  },

  async down (queryInterface) {
    await queryInterface.bulkDelete('State', null, {});
    await queryInterface.bulkDelete('City', null, {});
  }
};
