'use strict';
const Role = require('../models/index.js').Role
const User = require('../models/index.js').User
const bcrypt = require("bcrypt")

const users = [
  {
    name: "Marco",
    lastName: "Hawa",
    email: "admin@kanpai.mx",
    password: "KanpaiTesting1!",
    roles: ['ADMIN', 'USER'],
  },
  {
    name: "Bernardo",
    lastName: "Barragan",
    email: "provider@kanpai.mx",
    password: "KanpaiTesting1!",
    roles: ['PROVIDER', 'USER'],
  },
  {
    name: "Hernan",
    lastName: "Dominguez",
    email: "user@kanpai.mx",
    password: "KanpaiTesting1!",
    roles: ['USER'],
  },
]

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up() {
    for (let user of users) {
      // Hashing password before saving it
      user.password = await bcrypt.hash(user.password, 12)
      // Looking for roles in db based on the roles described in array
      const roles = await Role.findAll({ where: { nameId: user.roles } })
      // Creating user instance
      user = await User.create(user)
      // Setting roles previously searched
      await user.setRoles(roles);
      // Saving permissions for login
      user.isActive = true;
      user.isVerified = true;
      await user.save();
    }
  },

  async down(queryInterface) {
    await queryInterface.bulkDelete('Users', null, {});
  }
};
