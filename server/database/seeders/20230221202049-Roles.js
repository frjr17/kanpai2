'use strict';
const { randomUUID } = require("crypto");
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface) {
    await queryInterface.bulkInsert('Roles', [
      {
        id: randomUUID(),
        name: "Usuario",
        nameId: "USER",
        description: "Usuario de Kanpai",
      },
      {
        id: randomUUID(),
        name: "Proveedor",
        nameId: "PROVIDER",
        description: "Proveedor de Kanpai",
      },
      {
        id: randomUUID(),
        name: "Administrador",
        nameId: "ADMIN",
        description: "Administrador de Kanpai",
      },

    ])


  },

  async down(queryInterface) {
    await queryInterface.bulkDelete('Roles', null, {})
  }
};
