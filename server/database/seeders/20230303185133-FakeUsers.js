'use strict';
const { faker } = require("@faker-js/faker/locale/es_MX");
const bcrypt = require("bcrypt")
const Role = require("../models").Role
const User = require("../models").User
const State = require("../models").State
const _ = require('lodash');
const { Op } = require("sequelize");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up() {
    for (let i = 0; i < 50; i++) {
      if (process.env.NODE_ENV === 'production') return
      let user = {};
      user.password = 'KanpaiTesting1!';
      user.profileImg = faker.internet.avatar();
      user.name = faker.name.firstName();
      user.lastName = faker.name.lastName();
      user.email = 'faker' + faker.internet.email(user.name.toLowerCase(), user.lastName.toLowerCase());
      user.phoneNumber = faker.phone.number("##########");
      user.isActive = true;
      user.isVerified = true;

      // getting states
      const states = await State.findAll();
      const state = states[_.random(states.length - 1)]
      user.state = states[_.random(states.length - 1)].name
      const cities = await state.getCities();
      user.city = cities[_.random(cities.length - 1)].name

      // Hashing password before saving it
      user.password = await bcrypt.hash(user.password, 12)
      // Looking for roles in db based on the roles described in array
      const roles = await Role.findAll({ where: { nameId: 'USER' } })
      // Creating user instance
      user = await User.create(user)
      // Setting roles previously searched
      await user.setRoles(roles);
      // Saving permissions for login
      user.isActive = true;
      user.isVerified = true;
      await user.save();
    }
  },

  async down(queryInterface) {
    await queryInterface.bulkDelete('Users', { email: { [Op.startsWith]: 'faker' } }, {});
  }
};
