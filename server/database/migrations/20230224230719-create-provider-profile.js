'use strict';


/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('ProviderProfiles', {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true
      },
      hasRfc: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      rfcType: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      rfc: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      curp: {
        type: Sequelize.STRING,
        allowNull: true
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      proofOfAddreessImg: {
        type: Sequelize.STRING,
      },
      city: {
        type: Sequelize.STRING,
        allowNull: false
      },
      state: {
        type: Sequelize.STRING,
        allowNull: false
      },
      mayorsOffice: {
        type: Sequelize.STRING,
      },
      termsAndConditions: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      postalCode: {
        type: Sequelize.INTEGER,
      },
      isActive: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      userId: {
        type: Sequelize.UUID
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface) {
    await queryInterface.dropTable('ProviderProfiles');
  }
};