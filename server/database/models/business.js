'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Business extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.ProviderProfile, {
        as: "providerProfile",
        foreignKey: "providerProfileId",
        foreignKeyConstraint: true
      })
    }
  }
  Business.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false
    },
    phone: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    line: {
      type: DataTypes.STRING,
      allowNull: false
    },

    logoImg: {
      type: DataTypes.STRING,
    }
  }, {
    sequelize,
    modelName: 'Business',
  });
  return Business;
};