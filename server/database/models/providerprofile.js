'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProviderProfile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.User, {
        as: "user",
        foreignKey: "userId",
        foreignKeyConstraint: true
      })

      this.hasOne(models.Business, {
        as: 'business',
        foreignKey: 'providerProfileId',
        foreignKeyConstraint: true,
      })
      this.hasOne(models.Delegate, {
        as: "delegate",
        foreignKey: 'providerProfileId',
        foreignKeyConstraint: true,
      })
    }
  }
  ProviderProfile.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    userId: DataTypes.UUID,

    hasRfc: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    rfcType: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    rfc: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    curp: {
      type: DataTypes.STRING,
      allowNull: true
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    city: {
      type: DataTypes.STRING,
      allowNull: false
    },
    state: {
      type: DataTypes.STRING,
      allowNull: false
    },
    mayorsOffice: {
      type: DataTypes.STRING,
    },
    proofOfAddreessImg: {
      type: DataTypes.STRING,
    },
    termsAndConditions: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    postalCode: {
      type: DataTypes.INTEGER,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  }, {
    sequelize,
    modelName: 'ProviderProfile',
  });
  return ProviderProfile;
};