'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Delegate extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.ProviderProfile, {
        as: "providerProfile",
        foreignKey: "providerProfileId",
        foreignKeyConstraint: true
      })
    }
  }
  Delegate.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    phone: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    documentImage: {
      type: DataTypes.TEXT,
    }
  }, {
    sequelize,
    modelName: 'Delegate',
  });
  return Delegate;
};