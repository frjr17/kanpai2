<img src="./../utils/images/logo.png" alt="drawing" width="100" />

# **_Kanpai Project - Database_** 🔥

Here, all database configuration is stored.

## [**Sequelize Configuration**](config/config.json)

One file stored in the config folder contains all routes for the different databases:

1. The development database is stored in Site Ground.
2. The test and production database is stored in AWS.
3. The production database is not written directly, instead, it is provided via env route.

## [**Firebase Configuration**](config/firebaseConfig.js)

It was decided to store firebase configuration here because the authentication flow also store users. So, this folder would be a great place for having this file

*This is the only firebase configuration file in database folder, the rest is sequelize data*

## [**Migrations**](migrations/)

In this folder all models structure gets configured to get uploaded to database via sequelize-cli. The files stored in this folder are created and activated via **sequelize-cli**.

## [**Models**](models/)

Here, all models are created and handled. Also, it manages relations between these the table models and columns validations.

## [**Seeders**](seeders/)

The seeders folder contains files, generated via sequelize-cli, that manages to seed foundational app data.



