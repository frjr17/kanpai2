const admin = require("firebase-admin");

if (process.env.NODE_ENV !== 'production') {
    // Adding env variables
    require('dotenv').config();
}

const serviceAccount = JSON.parse(process.env.FIREBASE_CREDENTIALS)


if (!admin.apps.length) {

    module.exports = admin.initializeApp({
        credential: admin.credential.cert(serviceAccount)
    }, 'serverAdmin')
} else {
    module.exports = admin.apps[0]
}