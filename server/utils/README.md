<img src="./utils/images/logo.png" alt="drawing" width="100" />

# **_Kanpai Project - Utils_** 🔥

In this folder, every posible neccesity is stored. It's splitted in the following folders:

## **Constants**

The constants folder contains variables or data that is constantly and repeatedly used.

## **Email Templates**

All email templates, written in handlebars structuring, are written here.

## **Functions**

Every posible function needed is here

## **Images**

If we need to send some static data (like an image or svg), it would be here.