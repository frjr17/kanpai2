import { randomBytes } from "crypto";

export default class GlobalFunctions {
    static async createNewToken(bytes = 25) {
        const token = await randomBytes(bytes)
        return token.toString("hex")
    }
}