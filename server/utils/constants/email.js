import nodemailer from 'nodemailer';
import hbs from 'nodemailer-express-handlebars';

if (process.env.NODE_ENV !== 'production') {
    // Adding env variables
    require('dotenv').config();
}

let transporter = nodemailer.createTransport({
    host: 'gmadm1012.siteground.biz',
    port: 465,
    secure: true,
    auth: {
        user: process.env.EMAIL,
        pass: process.env.PASSWORD
    }
});

transporter.use('compile', hbs({
    viewEngine: {
        extName: '.handlebars',
        partialsDir: './server/utils/emailTemplates',
        layoutsDir: './server/utils/emailTemplates',
        defaultLayout: '',
    },
    viewPath: './server/utils/emailTemplates',
    extName: '.handlebars'
}));

export { transporter };