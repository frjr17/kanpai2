import { S3 } from "aws-sdk";

export const frontendUrl = process.env.NODE_ENV === 'production' ? process.env.FRONTEND_URL : "http://localhost:3000";

export const mayorsOffices = [
    'Álvaro Obregón',
    'Azcapotzalco',
    'Benito Juárez',
    "Benito Juárez",
    "Coyoacán",
    "Cuajimalpa de Morelos",
    "Cuauhtémoc",
    "Gustavo A. Madero",
    "Iztacalco",
    "Iztapalapa",
    "Magdalena Contreras",
    "Miguel Hidalgo",
    "Milpa Alta",
    "Tláhuac",
    "Tlalpan",
    "Venustiano Carranza",
    "Xochimilco"
];

export const s3 = new S3({
    region: process.env.AWS_REGION,
    credentials: {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    }
})