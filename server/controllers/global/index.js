import { validationResult } from "express-validator";
import GlobalService from "./service";
export default class GlobalController {
    static initializeState(req, res, next) {
        try {
            req.state = {};
            return next()
        } catch (error) {
            console.log("State Error:", { error })
        }
    }

    static async sendState(req, res) {

        try {
            let { statusCode, ...state } = req.state;

            if (!statusCode) {
                statusCode = 200;
            }
            return res.status(statusCode).send(state);
        } catch (error) {
            console.log("Send state Error:", { error })
        }
    }

    // eslint-disable-next-line no-unused-vars
    static async errorController(err, req, res, next) {
        try {
            // eslint-disable-next-line no-unused-vars
            let { statusCode, error, ...state } = req.state;

            if (!statusCode) {
                statusCode = 400;
            }

            return res.status(statusCode).send({ ...state })
        } catch (error) {
            console.log("Error sending error.😢", { error })
        }
    }

    static async sendEmail(req, res, next) {
        try {
            const { sendEmail, emailData, ...state } = req.state;

            if (sendEmail) {
                const isEmailConnection = await GlobalService.verifyEmailConnection()

                if (isEmailConnection) {
                    await GlobalService.sendEmail(emailData)
                    req.state = state;
                } else {
                    req.state.message = 'No pudimos enviar el correo.'
                    return next(true);
                }
            }

            next()
        } catch (error) {
            console.log("Send email", { error })
        }
    }

    static async checkValidators(req, res, next) {
        try {
            let errors = validationResult(req);

            if (!errors.isEmpty()) {
                errors = errors.array();
                const results = {};
                errors.map(error => {
                    if (!results[error.param]) {
                        results[error.param] = []
                    }
                    results[error.param].push(error.msg);
                })

                req.state.message = 'Errores de validacion';
                req.state.errors = results
                req.state.statusCode = 400;
                return next(true);
            }
            next()
        } catch (error) {
            console.log("Send email", { error })
        }
    }

    static notFound(req, res) {
        res.status(404).send(req.url + " not found!")
    }
}