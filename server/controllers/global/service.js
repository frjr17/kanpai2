import { transporter } from "../../utils/constants/email";




export default class GlobalService {

    static async verifyEmailConnection() {
        const emailConnection = await new Promise((resolve, reject) => {
            transporter.verify(function (error, success) {
                if (error) {
                    reject(error);
                } else {
                    resolve(success);
                }
            });
        })

        return emailConnection

    }



    static async sendEmail(emailData) {
        const from = `"Kanpai" <${process.env.EMAIL}>`
        const { to, context, subject, text, template } = emailData

        return await new Promise((resolve, reject) => {
            transporter.sendMail({
                from,
                to,
                subject,
                text,
                template,
                context
            }, (err, info) => {
                if (err) {
                    console.error(err);
                    reject(err);
                } else {
                    resolve(info);
                }
            })
        })
    }


}