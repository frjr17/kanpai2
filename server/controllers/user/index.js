import UserService from "./service";

export default class UserController {
    // getProfile
    static async getProfile(req, res, next) {
        const result = await UserService.getProfile(req.state.token);
        req.state = { ...req.state, ...result }

        if (req.state.error) {
            return next(req.state)
        }

        return next();
    }

    // updateProfile
    static async updateProfile(req, res, next) {
        const result = await UserService.updateProfile(req.state.token, req.body);
        req.state = { ...req.state, ...result }

        if (req.state.error) {
            return next(req.state)
        }

        return next();
    }

    // updatePassword 
    static async updatePassword(req, res, next) {
        const result = await UserService.updatePassword(req.state.token, req.body.currentPassword, req.body.newPassword);
        req.state = { ...req.state, ...result }

        if (req.state.error) {
            return next(req.state)
        }

        return next();
    }
}