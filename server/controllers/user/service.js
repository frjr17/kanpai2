import { LoginToken } from "../../database/models";
import { Role } from "../../database/models";
import { User } from "../../database/models";
import bcrypt from "bcrypt";

export default class UserService {
    // getProfile
    static async getProfile(token) {
        const result = {}
        try {
            token = await LoginToken.findOne({ where: { token } })

            const toInclude = ['name', 'lastName', 'email', 'phoneNumber', 'state', 'city', 'profileImg', 'isGoogleAccount', 'isFacebookAccount', 'createdAt']
            const roleInclude = [{ model: Role, as: 'roles', exclude: ['id'], attributes: ['name', 'nameId', 'description'], through: { attributes: [] } }]
            const user = await token.getUser({ attributes: toInclude, include: roleInclude });

            // Success Details
            result.message = 'Perfil conseguido!';
            result.user = user.toJSON();
        } catch (error) {
            // Logging error
            console.error("getProfile Service", { error })

            // Sending error details to state.
            result.message = 'Lo sentimos, hubo un error al conseguir el perfil del usuario.';
            result.error = error
        }
        return result
    }

    // updateProfile
    static async updateProfile(token, newUserData) {
        const result = {}
        try {
            token = await LoginToken.findOne({ where: { token } })

            const user = await token.getUser();
            await User.update(newUserData, { where: { id: user.id } })

            // Success Details
            result.message = 'Perfil actualizado con éxito!';
        } catch (error) {
            // Logging error
            console.error("updateProfile Service", { error })

            // Sending error details to state.
            result.message = 'Lo sentimos, hubo un error al actualizar el usuario.';
            result.error = error
        }
        return result
    }

    // updatePassword 
    static async updatePassword(token, password, newPassword) {
        const result = {}
        try {
            token = await LoginToken.findOne({ where: { token } })

            const user = await token.getUser();
            const isPasswordEqual = await bcrypt.compare(newPassword, user.password);
            const isOldPasswordEqual = await bcrypt.compare(password, user.password);

            if (!isOldPasswordEqual) {
                result.message = 'La contraseña actual es incorrecta.';
                result.statusCode = 400;
                result.error = true;
                return result
            }

            if (isPasswordEqual) {
                result.message = 'La nueva contraseña no puede ser igual que la anterior.'
                result.statusCode = 400;
                result.error = true;
                return result
            }

            user.password = await bcrypt.hash(newPassword, 12);
            await user.save();

            // Success Details
            result.message = 'Contraseña actualizada con exito!';
        } catch (error) {
            // Logging error
            console.error("updatePassword Service", { error })

            // Sending error details to state.
            result.message = 'Lo sentimos, hubo un error al actualizar tu contraseña.';
            result.error = error
        }
        return result
    }
} 