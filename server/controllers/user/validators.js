import { body } from "express-validator";

export default class UserValidators {
    // updateProfile
    static updateProfile() {
        const name = body('name')
            .if(body('name').notEmpty())
            .isLength({ min: 3 })
            .withMessage("El nombre debe tener al menos 3 caracteres.")
            .isAlpha('es-ES')
            .withMessage("El nombre no puede contener numeros")
            .trim()

        const lastName = body('lastName')
            .if(body('lastName').notEmpty())
            .isLength({ min: 3 })
            .withMessage("El apellido debe tener al menos 3 caracteres.")
            .isAlpha('es-ES')
            .withMessage("El apellido no puede contener numeros")
            .trim()

        const phoneNumber = body("phoneNumber")
            .if(body("phoneNumber").notEmpty())
            .isInt()
            .withMessage("El numero de teléfono solo debe contener numeros enteros.")
            .isMobilePhone("es-MX")
            .withMessage("El numero de telefono no corresponde a la región de México.")

        const state = body('state')
            .if(body('state').notEmpty())
            .isLength({ min: 3, max: 60 })
            .withMessage("El estado debe tener entre 3 y 60 caracteres.")
            .isAlpha()
            .withMessage("El estado no puede contener numeros")


        const city = body('city')
            .if(body('city').notEmpty())
            .isLength({ min: 3, max: 60 })
            .withMessage("El estado debe tener entre 3 y 60 caracteres.")
            .isAlpha()
            .withMessage("La ciudad no puede contener numeros")


        return [name, lastName, phoneNumber, state, city]
    }

    // updatePassword
    static updatePassword() {

        const currentPassword = body('currentPassword')
            .notEmpty()
            .withMessage("Debes escribir la contraseña actual")

        const newPassword = body('newPassword')
            .notEmpty()
            .withMessage("Debes escribir la nueva contraseña")
            .isStrongPassword({ minLength: 8, minLowercase: 1, minUppercase: 1, minNumbers: 1, minSymbols: 1 })
            .withMessage("La nueva contraseña debe tener 8 caracteres mínimo, un numero, una mayúscula, una minúscula y un símbolo.")
            .custom((newPassword, { req }) => {
                if (newPassword === req.body.currentPassword) {
                    return Promise.reject("La nueva contraseña no debe ser igual a la anterior.")
                }
                return true
            })

        return [currentPassword, newPassword];
    }
}