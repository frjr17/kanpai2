import { State } from '../../database/models';
import { City } from '../../database/models';

export default class ServicesService {

    static async getCitiesAndStates() {
        const result = {}
        try {
            let states = await State.findAll({ include: [{ model: City, as: 'cities', attributes: ['name'] }], attributes: { exclude: ['createdAt', 'updatedAt', 'id'] } })
            states = states.map(state => ({ name: state.name, cities: state.cities.map(city => city.name) }))
            // Success Details
            result.states = states
            result.message = 'Ciudades y estados conseguidos';
        } catch (error) {
            // Logging error
            console.error("getCitiesAndStates Service", { error })

            // Sending error details to state.
            result.message = 'Lo sentimos, hubo un error al conseguir los estados y ciudades.';
            result.error = error
        }
        return result
    }
}