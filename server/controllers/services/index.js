import ServicesService from "./service";


export default class ServicesController {
    // getCitiesAndStates
    static async getCitiesAndStates(req, res, next) {
        const result = await ServicesService.getCitiesAndStates();
        req.state = { ...req.state, ...result }

        if (req.state.error) {
            return next(req.state)
        }

        return next();
    }
}