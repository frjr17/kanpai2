import ProviderService from "./service";

export default class ProviderController {
    // register Provider
    static async register(req, res, next) {
        const result = await ProviderService.register(req.body, req.state.token);
        req.state = { ...req.state, ...result }

        if (req.state.error) {
            return next(req.state)
        }

        return next();
    }
    static async verifyProvider(req, res, next) {
        const result = await ProviderService.verifyProvider(req.body);
        req.state = { ...req.state, ...result }

        if (req.state.error) {
            return next(req.state)
        }

        return next();
    }

    static async uploadTempFile(req, res, next) {
        const result = await ProviderService.uploadTempFile(req.body);
        req.state = { ...req.state, ...result }

        if (req.state.error) {
            return next(req.state)
        }

        return next();
    }

    static async getAll(req, res, next) {
        const result = await ProviderService.getAll();
        req.state = { ...req.state, ...result }

        if (req.state.error) {
            return next(req.state)
        }

        return next();
    }
}