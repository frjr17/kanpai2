import { sequelize } from "../../database/models";
import { LoginToken } from "../../database/models";
import { ProviderProfile, Delegate, Business } from "../../database/models";
import { s3 } from "../../utils/constants";

export default class ProviderService {

    static async register(data, token) {
        const result = {}
        const transaction = await sequelize.transaction();
        try {
            let { business, delegate, ...providerProfile } = data;

            token = await LoginToken.findOne({ where: { token } }, { transaction })
            const user = await token.getUser();
            await user.createProviderProfile(providerProfile);

            providerProfile = await user.getProviderProfile();
            await providerProfile.createBusiness(business, { transaction })
            await providerProfile.createDelegate(delegate, { transaction })

            await transaction.commit();
            // Success Details
            result.message = 'Cuenta registrada, enviamos tus datos a administración para que puedan validar tu solicitud';
        } catch (error) {
            await transaction.rollback()
            // Logging error
            console.error("register Service", { error })
            // Sending error details to state.
            result.message = 'Lo sentimos, hubo un error al registrar tu cuenta de proveedor.';
            result.error = error
        }
        return result
    }

    static async verifyProvider(data) {
        const result = {}
        try {
            const { providerId, isAccepted } = data;
            const provider = await ProviderProfile.findOne({ id: providerId });

            if (isAccepted) {
                if (provider.isActive) {
                    result.message = 'Este proveedor ya fue aceptado!'
                    result.statusCode = 400
                    result.error = true;
                    return result
                }

                provider.isActive = true;
                await provider.save()
                // Success Details
                result.message = 'Proveedor Verificado';
            } else {
                const delegate = await provider.getDelegate();
                const business = await provider.getBusiness();

                await delegate.destroy();
                await business.destroy();
                await provider.destroy();
                result.message = "Proveedor Eliminado!"
            }

        } catch (error) {
            // Logging error
            console.error("verifyProvider Service", { error })

            // Sending error details to state.
            result.message = 'Lo sentimos, hubo un error al verificar al proveedor.';
            result.error = error
        }
        return result
    }

    static async uploadTempFile(file) {
        const result = {}
        try {
            const { name, type } = file

            const params = {
                Bucket: process.env.AWS_S3_BUCKET_TEMP,
                Key: name,
                Expires: 600,
                ContentType: type,
                ACL: "public-read",

            };

            const url = await s3.getSignedUrlPromise('putObject', params)

            // Success Details
            result.url = url
            result.message = 'File Uploaded in temp.';
        } catch (error) {
            // Logging error
            console.error("uploadTempFile Service", { error })

            // Sending error details to state.
            result.message = 'Lo sentimos, hubo un error al subir el archivo temporal.';
            result.error = error
        }
        return result
    }

    static async getAll() {
        const result = {}
        try {
            const include = [{ model: Delegate, as: "delegate", attributes: ['name', 'phone', 'documentImage'] }]
            include.push({ model: Business, as: "business", attributes: ['name', 'phone', 'email', 'line', 'logoImg'] })
            const providers = await ProviderProfile.findAll({ include: include });

            result.providers = providers;

            // Success Details
            result.message = 'Proveedores Conseguidos!';
        } catch (error) {
            // Logging error
            console.error("getAll Service", { error })

            // Sending error details to state.
            result.message = 'Lo sentimos, hubo un error al obtener a todos los proveedores.';
            result.error = error
        }
        return result
    }
}