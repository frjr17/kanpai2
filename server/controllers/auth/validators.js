import { default as axios } from "axios";
import { body, query } from "express-validator";

const email = (validationChain = body("email")) => validationChain
    .notEmpty()
    .withMessage("La direccion de correo no puede estar vacía.")
    .isEmail()
    .withMessage("La direccion de correo es inválida.")
    .normalizeEmail()

export default class AuthValidators {
    // register
    static register() {
        const name = body('name')
            .notEmpty()
            .withMessage("El nombre no puede estar vacio")
            .isAlpha('es-ES')
            .withMessage("El nombre no debe contener numeros.")
            .isLength({ min: 2 })
            .withMessage("El nombre debe tener mínimo 2 caracteres.")
            .trim()

        const lastName = body('lastName')
            .notEmpty()
            .withMessage("El apellido no puede estar vacio")
            .isAlpha('es-ES')
            .withMessage("El apellido no debe contener numeros.")
            .isLength({ min: 2 })
            .withMessage("El apellido debe tener mínimo 2 caracteres.")
            .trim()

        const password = body("password")
            .if(body('isGoogleAccount').notEmpty())
            .if(body('isFacebookAccount').notEmpty())
            .notEmpty()
            .withMessage("La contraseña no puede estar vacía")
            .isStrongPassword({ minLength: 8, minLowercase: 1, minUppercase: 1, minNumbers: 1, minSymbols: 1 })
            .withMessage("La contraseña debe contar con mínimo 8 caracteres, un caracter minúscula, uno mayúscula, un número y un símbolo.")

        const confirmPassword = body('confirmPassword')
            .if(body('isGoogleAccount').notEmpty())
            .if(body('isFacebookAccount').notEmpty())
            .notEmpty()
            .withMessage("Debes escribir la confirmación de contraseña")
            .custom((confirmPassword, { req }) => {
                if (confirmPassword !== req.body.password) {
                    return Promise.reject("Las contraseñas no coinciden.")
                }
                return true
            })

        const captchaToken = body("captchaToken")
            .if(body('isGoogleAccount').notEmpty())
            .if(body('isFacebookAccount').notEmpty())
            .notEmpty()
            .withMessage("No puedes iniciar sesion sin el captcha resuelto")
            .custom(async (captcha) => {
                const secret = process.env.RECAPTCHA_SECRET_KEY;
                const axiosResponse = await axios({
                    method: "POST",
                    url: `https://www.google.com/recaptcha/api/siteverify`,
                    params: {
                        secret: secret,
                        response: captcha
                    }
                });

                if (!axiosResponse.data.success) {
                    return Promise.reject("El captcha es inválido")
                }

                return true;

            })
        return [name, lastName, email(), password, confirmPassword, captchaToken];
    }

    // login
    static login() {
        const password = body("password")
            .if(body('isGoogleAccount').notEmpty())
            .if(body('isFacebookAccount').notEmpty())
            .notEmpty()
            .withMessage("La contraseña no puede estar vacía")

        const captchaToken = body("captchaToken")
            .if(body('isGoogleAccount').notEmpty())
            .if(body('isFacebookAccount').notEmpty())
            .notEmpty()
            .withMessage("No puedes iniciar sesion sin el captcha resuelto")
            .custom(async (captcha) => {
                const secret = process.env.RECAPTCHA_SECRET_KEY;
                const axiosResponse = await axios({
                    method: "POST",
                    url: `https://www.google.com/recaptcha/api/siteverify`,
                    params: {
                        secret: secret,
                        response: captcha
                    }
                });

                if (!axiosResponse.data.success) {
                    return Promise.reject("El captcha es inválido")
                }

                return true;

            })
        return [email(), password, captchaToken]
    }

    // resetPasswordRequest
    static resetPasswordRequest() {
        return [email(query("email"))]
    }

    // resetPassword
    static resetPassword() {
        const password = body("password")
            .notEmpty()
            .withMessage("Debes escribir la contraseña actual")
            .isStrongPassword({ minLength: 8, minLowercase: 1, minUppercase: 1, minNumbers: 1, minSymbols: 1 })
            .withMessage("La contraseña debe contar con mínimo 8 caracteres, un caracter minúscula, uno mayúscula, un número y un símbolo.")


        const confirmPassword = body('confirmPassword')
            .notEmpty()
            .withMessage("Debes escribir la confirmación de contraseña")
            .custom((confirmPassword, { req }) => {
                if (confirmPassword !== req.body.password) {
                    return Promise.reject("Las contraseñas no coinciden.")
                }
                return true
            })

        return [password, confirmPassword]
    }
}