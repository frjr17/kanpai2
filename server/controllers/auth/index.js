import AuthService from "./service";

export default class AuthController {
    // Verify email for register
    static async verifyEmailToken(req, res, next) {
        const result = await AuthService.verifyEmailToken(req.query.token)
        req.state = { ...req.state, ...result }

        if (req.state.error) {
            return next(req.state)
        }

        return next()
    }

    // Verify resetPassword token
    static async verifyResetPasswordToken(req, res, next) {
        const result = await AuthService.verifyResetPasswordToken(req.query.token)
        req.state = { ...req.state, ...result }

        if (req.state.error) {
            return next(req.state)
        }

        return next()
    }


    // Validate login token
    static async verifyLoginToken(req, res, next) {
        const result = await AuthService.verifyLoginToken(req.query.token)
        req.state = { ...req.state, ...result }

        if (req.state.error) {
            return next(req.state)
        }

        return next()
    }


    // register
    static async register(req, res, next) {
        let result;

        if (req.body.isGoogleAccount || req.body.isFacebookAccount) {
            const isGoogle = req.body.isGoogleAccount
            result = await AuthService.firebaseRegister(req.body, isGoogle)
        } else {
            result = await AuthService.register(req.body);
        }

        req.state = { ...req.state, ...result }

        if (req.state.error) {
            return next(req.state)
        }

        return next();
    }

    // login
    static async login(req, res, next) {
        let result;
        if (req.body.isGoogleAccount || req.body.isFacebookAccount) {
            result = await AuthService.firebaseLogin(req.body)
        } else {
            result = await AuthService.login(req.body);
        }

        req.state = { ...req.state, ...result }

        if (req.state.error) {
            return next(req.state)
        }

        return next();
    }

    // resetPasswordRequest
    static async resetPasswordRequest(req, res, next) {
        const result = await AuthService.resetPasswordRequest(req.query.email);
        req.state = { ...req.state, ...result }

        if (req.state.error) {
            return next(req.state)
        }

        return next();
    }

    // resetPassword 
    static async resetPassword(req, res, next) {
        const result = await AuthService.resetPassword(req.body.password, req.headers.authorization)
        req.state = { ...req.state, ...result }

        if (req.state.error) {
            return next(req.state)
        }

        return next()
    }


    // signOut
    static async signOut(req, res, next) {
        const result = await AuthService.signOut(req.query.token);
        req.state = { ...req.state, ...result }

        if (req.state.error) {
            return next(req.state)
        }

        return next();
    }

    // authorize
    static authorize(...rolesAdmitted) {
        if (!rolesAdmitted.length) {
            rolesAdmitted = ['USER']
        }
        return async function (req, res, next) {
            const result = await AuthService.authorize(req.headers.authorization, rolesAdmitted);
            req.state = { ...req.state, ...result }

            if (req.state.error) {
                return next(req.state)
            }

            return next();
        }
    }

}