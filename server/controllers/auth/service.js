import bcrypt from "bcrypt";
import { frontendUrl } from "../../utils/constants";
import { User } from "../../database/models";
import { Role } from "../../database/models";
import { LoginToken, VerificationToken, ResetPasswordToken, sequelize } from "../../database/models";


import GlobalFunctions from "../../utils/functions";
import admin from "../../database/config/firebaseConfig";

export default class AuthService {

    // Verify resetPassword token
    static async verifyResetPasswordToken(token) {
        const result = {}
        try {
            token = await ResetPasswordToken.findOne({ where: { token } });

            if (!token || token.isUsed) {
                result.message = 'Token invalido!'
                result.statusCode = 400
                result.error = true;
                return result
            }

            // Success Details
            result.message = 'Token verificado con exito!';
        } catch (error) {
            // Logging error
            console.error("verifyResetPasswordToken Service", { error })

            // Sending error details to state.  
            result.message = 'Lo sentimos, hubo un error al verificar el token de cambio de contraseña.';
            result.error = error
        }
        return result
    }

    // Verify login token
    static async verifyLoginToken(token) {
        const result = {}
        try {
            // Extracting token from db
            token = await LoginToken.findOne({ where: { token } })

            // If there's no token, throwing error gracefully...
            if (!token) {
                result.message = 'Token Invalido!'
                result.statusCode = 400;
                result.error = true
                return result
            }

            // Extracting token user...
            const includeRoles = { model: Role, as: "roles", attributes: ['name', 'nameId', 'description'], through: { attributes: [] } };
            const user = await token.getUser({ include: [includeRoles] });

            // Success details.
            result.message = 'Verificación exitosa!'
            result.user = user;
        } catch (error) {
            // Logging error
            console.error("verifyLoginToken Service", { error })

            // Sending error details to state.
            result.message = 'Lo sentimos, hubo un error al verificar el inicio de sesion.';
            result.error = error
        }
        return result
    }

    // register
    static async register(data) {
        const result = {};

        // Starting transaction
        const transaction = await sequelize.transaction();

        try {
            // Extracting user data
            let { name, lastName, email, password } = data;
            password = await bcrypt.hash(password, 12);

            // If there's a user, throwing error gracefully
            const isUser = await User.findOne({ where: { email } })
            if (isUser) {
                result.message = 'El correo ya fue utilizado'
                result.statusCode = 400
                result.error = true;
                return result
            }

            // Creating the new user
            const newUser = await User.create({
                name,
                lastName,
                email,
                password,
            }, { transaction })

            // Finding userRole and setting it up
            const userRole = await Role.findOne({ where: { nameId: "USER" }, transaction });
            await newUser.setRoles([userRole], { transaction });

            // Creating new token and assigning it.
            let token = await GlobalFunctions.createNewToken();
            await newUser.createVerificationToken({ token }, { transaction });
            // Closing transaction and storing it to database
            transaction.commit();

            // Adding emailData props for sending verifiaction email.
            result.sendEmail = true;
            result.emailData = {
                to: email,
                subject: 'Verificación de cuenta',
                text: 'Por favor verifica tu email',
                template: "activateAccount",
                context: {
                    email,
                    name: name + " " + lastName,
                    referer: frontendUrl,
                    token
                }
            };

            // Results
            result.message = `Hemos enviado un correo a ${email} para verificar la cuenta.`;
            result.statusCode = 200;
        } catch (error) {
            // Ending transaction
            await transaction.rollback();

            // Logging error
            console.error("Register Service", { error })

            // Sending error details to state.
            result.message = 'Lo sentimos, hubo un error al registrarte';
            result.error = error
        }

        return result
    }

    // firebase register
    static async firebaseRegister(data, isGoogle = true) {
        const result = {};
        // Starting transaction
        const transaction = await sequelize.transaction();

        try {
            // Extracting user data
            let { name, lastName, email, profileImg, token } = data;

            const serviceProp = isGoogle ? "isGoogleAccount" : 'isFacebookAccount'

            try {
                await admin.auth().verifyIdToken(token)
            } catch (error) {
                result.message = 'Token invalido';
                result.statusCode = 403
                result.error = true
                return result
            }

            // If there's a user, throwing error gracefully
            const isUser = await User.findOne({ where: { email } })
            if (isUser) {
                result.message = 'Esta cuenta de google ya fue registrada'
                result.statusCode = 400
                result.error = true;
                return result
            }

            // Creating the new user
            const newUser = await User.create({
                name,
                lastName,
                email,
                profileImg,
                [serviceProp]: true,
                isVerified: true,
            }, { transaction })

            // Finding userRole and setting it up
            const userRole = await Role.findOne({ where: { nameId: "USER" }, transaction });
            await newUser.setRoles([userRole], { transaction });

            await newUser.createLoginToken({ token }, { transaction })

            // Closing transaction and storing it to database
            transaction.commit();

            // Adding emailData props for sending verifiaction email.
            result.sendEmail = true;
            result.emailData = {
                to: email,
                subject: 'Bienvenido a Kanpai!',
                text: 'Bienvenido a Kanpai!',
                template: "userWelcome",
                context: {
                    email,
                    name: name + " " + lastName,
                    referer: frontendUrl,
                }
            };

            // Results
            result.message = `Cuenta Registrada! Hemos enviado un correo de bienvenida a ${email}.`;
            result.statusCode = 200;
        } catch (error) {
            // Ending transaction
            await transaction.rollback();

            // Logging error
            console.error("Register Service", { error })

            // Sending error details to state.
            result.message = 'Lo sentimos, hubo un error al registrarte';
            result.error = error
        }

        return result
    }

    // login
    static async login(loginData) {
        const result = {}
        try {
            // Extracting login data
            let { email, password } = loginData;

            const toExclude = ['updatedAt', 'isGoogleAccount', 'isFacebookAccount', 'isActive', 'isVerified', 'isProvider'];
            const includeRoles = { model: Role, as: "roles", attributes: ['name', 'nameId', 'description'], through: { attributes: [] } };

            // Finding user
            const user = await User.findOne({ where: { email }, include: [includeRoles], attributes: { exclude: toExclude } });

            if (!user) {
                result.message = 'Email Incorrecto!';
                result.statusCode = 400;
                result.error = true;
                return result;
            }

            // Comparing extracted password with db password
            const isPasswordMatch = await bcrypt.compare(password, user.password);

            // If passwords don't match, throwing error gracefully
            if (!isPasswordMatch) {
                result.message = 'La contraseña es incorrecta';
                result.statusCode = 400;
                result.error = true;
                return result;
            }

            // Creating error and saving it to db
            let token = await GlobalFunctions.createNewToken();
            await user.createLoginToken({ token });

            // Showing results.
            result.message = 'Inicio de sesion exitoso';
            result.token = token;
            result.user = { ...user.toJSON(), password: undefined };
        } catch (error) {
            // Logging error
            console.error("Login Service", { error })

            // Sending error details to state.
            result.message = 'Lo sentimos, hubo un error al iniciar sesion';
            result.error = error
        }

        return result
    }

    // firebase login
    static async firebaseLogin(loginData) {
        const result = {}
        try {
            // Extracting login data
            const { email, token } = loginData;

            try {
                await admin.auth().verifyIdToken(token)
            } catch (error) {
                result.message = 'Token invalido';
                result.statusCode = 403
                result.error = true
                return result
            }

            // Finding user
            const user = await User.findOne({ where: { email } });

            if (!user) {
                result.message = 'Esta cuenta no ha sido registrada aún';
                result.statusCode = 400;
                result.error = true;
                return result;
            }

            // Creating error and saving it to db
            await user.createLoginToken({ token });

            // Showing results.
            result.message = 'Inicio de sesion exitoso';
            result.token = token;
        } catch (error) {
            // Logging error
            console.error("Login Service", { error })

            // Sending error details to state.
            result.message = 'Lo sentimos, hubo un error al iniciar sesion';
            result.error = error
        }

        return result
    }

    // resetPasswordRequest
    static async resetPasswordRequest(email) {
        const result = {}
        try {
            // Extracting user from db
            const user = await User.findOne({ where: { email } });

            // If there's no user, throwing error gracefully
            if (!user) {
                result.message = 'Este correo no se ha registrado.'
                result.statusCode = 400
                result.error = true;
                return result;
            }

            // Creating a new reset password token.
            const token = await GlobalFunctions.createNewToken();
            await user.createResetPasswordToken({ token });

            // Sending email for user verification.
            const { name, lastName } = user;
            result.sendEmail = true;
            result.emailData = {
                to: email,
                subject: 'Cambio de Contraseña',
                text: 'Cambio de Contraseña',
                template: "resetPassword",
                context: {
                    email,
                    name: name + " " + lastName,
                    referer: frontendUrl,
                    token
                }
            };

            // Success Details
            result.message = `Cambio de contraseña solicitado, enviamos un correo a ${email} para validar la solicitud.`;
        } catch (error) {
            // Logging error
            console.error("resetPasswordRequest Service", { error })

            // Sending error details to state.
            result.message = 'Lo sentimos, hubo un error al solicitar el cambio de contraseña.';
            result.error = error
        }
        return result
    }

    // resetPassword 
    static async resetPassword(newPassword, token) {
        const result = {}
        try {
            // Extracting token from db
            token = await ResetPasswordToken.findOne({ where: { token } })

            // If there's no token, throwing error gracefully
            if (!token) {
                result.message = 'Token invalido!'
                result.statusCode = 400;
                return result
            }

            // If token is already used, throwing error gracefully
            if (token.isUsed) {
                result.message = 'El token ya fue usado'
                result.statusCode = 400;
                return result
            }

            // Extracting user from token and verifying if new password is the same than the older
            const user = await token.getUser();
            const isNewPasswordEqual = await bcrypt.compare(newPassword, user.password);

            // if new password is the same than the older,throwing error gracefully
            if (isNewPasswordEqual) {
                result.message = 'La nueva contraseña no puede ser la misma que la anterior!'
                result.statusCode = 400;
                return result
            }

            //Saving new password
            user.password = await bcrypt.hash(newPassword, 12)
            await user.save();

            // Updating "isUsed" prop from token.
            token.isUsed = true;
            await token.save();

            // Success Details
            result.message = 'Cambio de contraseña exitoso! Ya puedes iniciar sesión';
        } catch (error) {
            // Logging error
            console.error("resetPassword Service", { error })

            // Sending error details to state.
            result.message = 'Lo sentimos, hubo un error al cambiar tu contraseña.';
            result.error = error
        }
        return result
    }

    // Verify email after register
    static async verifyEmailToken(token) {
        const result = {}
        try {
            // Extracting token from db
            token = await VerificationToken.findOne({ where: { token } })

            // If there's no token, or it's already used, throwing error gracefully.
            if (!token || token.isUsed) {
                result.message = 'El token es inválido';
                result.statusCode = 400;
                result.error = true;
                return result
            }

            // Changing "isUsed" prop to true and saving it.
            token.isUsed = true;
            await token.save();

            // Getting user from token
            const user = await token.getUser();

            // Changing "isVerified" prop to true, so user can access and saving it.
            user.isVerified = true;
            await user.save();

            // Extracting props from user to send a welcome mail.
            const { email, name, lastName } = user
            result.sendEmail = true;
            result.emailData = {
                to: email,
                subject: 'Bienvenido a Kanpai!',
                text: 'Bienvenido a Kanpai!',
                template: "userWelcome",
                context: {
                    email,
                    name: name + " " + lastName,
                    referer: frontendUrl,
                }
            };


            result.message = "Correo verificado exitosamente!"
        } catch (error) {
            // Logging error
            console.error("verifyEmail Service", { error })

            // Sending error details to state.
            result.message = 'Lo sentimos, hubo un error al verificar la cuenta';
            result.error = error
        }
        return result
    }

    // signOut
    static async signOut(token) {
        const result = {}
        try {
            // Extracting token from db
            token = await LoginToken.findOne({ where: { token } })

            // If there's no token in db, throwing error gracefully.
            if (!token) {
                result.message = "Token invalido!"
                result.statusCode = 400;
                result.error = true
                return result;
            }

            // Destroying token.
            await token.destroy();

            result.message = 'Sesion cerrada con exito!'
        } catch (error) {
            // Logging error
            console.error("verifyEmail Service", { error })

            // Sending error details to state.
            result.message = 'Lo sentimos, hubo un error al cerrar sesión';
            result.error = error
        }
        return result
    }

    // authorizeUser
    static async authorize(token, rolesAdmitted) {
        const result = {}
        try {
            // Extracting token from db
            token = await LoginToken.findOne({ where: { token } })

            // If there's no token in db, throwing error gracefully.
            if (!token) {
                result.message = "Token invalido!"
                result.statusCode = 400;
                result.error = true;
                return result;
            }

            const user = await token.getUser({ include: [{ model: Role, as: 'roles' }] })

            const isAdmitted = user.roles.filter(role => rolesAdmitted.includes(role.nameId)).length

            if (!isAdmitted) {
                result.message = "Acceso restringido"
                result.statusCode = 400;
                result.error = true;
                return result;
            }

            // Success Details
            result.message = 'Usuario autenticado!';
            result.token = token.token;
        } catch (error) {
            // Logging error
            console.error("authorize Service", { error })

            // Sending error details to state.
            result.message = 'Lo sentimos, hubo un error al autenticar al usuario.';
            result.error = error
        }
        return result
    }
}