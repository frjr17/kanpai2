<img src="./../utils/images/logo.png" alt="drawing" width="100" />

# **_Kanpai Project - Controllers_** 🔥

Here, all the server logic process begin to happen. Inside this folder, all interactions with database, AWS S3 buckets, firebase authentication, validators... are managed.

This is a extremely important function because, without it, the app won't work.

## **Structure**
The controllers structure functions like this.

* Each controller are stored in a folder.
* The controller splits into three main files:
  * **Index.js**: where the main controller files are stored.
  * **service.js**: where logic is store for the main controller.
  * **validators.js**: in case the controller need forms validation.


## [**Auth Controller**](auth/index.js) 

This controller handles:

1. Registering
2. Registering Validation
3. Login.
4. Login Validation
5. Reset Password.
6. Reset Password validation
7. Sign out.
8. Authorize. (for routes that require specific users.)

## [**Global Controller**](global/index.js)

This controller is a little bit different than the rest, because is handles more specific functions in the app middleware flow. These are:

1. Initialize State
2. Send State
3. Global Error controller
4. Email Sender
5. Validation Check

## [**User Controller**](user/index.js)

This controller handles:

1. User Profile
2. Profile Updates
3. Password Updates