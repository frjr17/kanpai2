<img src="./public/images/logo/logo%20-%20s.png" alt="drawing" width="100"/>

# **_Kanpai Project_** 🔥

This is the code used in Kanpai Web Application.

**Developing company:** [PDS Investments](inversionespds.com)

**_Developer:_** [frjr17 - Hernán Valencia](https://github.com/frjr17)

This project consists on a Web Store for users that need services for parties and unique events. It was built using [Next.js](https://nextjs.org/) framework, based on [React.js](https://reactjs.org/).

## **App Deployment Details**

App works exactly the same in production than in development, the only diference is the serverUrl prop. in Development mode, serverUrl repsond response to [localhost:5000](http://localhost:5000). But in production mode, serverUrl points to deployment API in amazon (_url not defined yet_).

In development and production mode, you've to take care of declaring the [env variables](#env-variables) necessaries for app functionlity.

## [**Dev Usage**](#dev-usage)

How to start frontend in development server.

1.  Clone this repo.
2.  Install dependancies

        npm install

3.  Add a .env.local file with all the neccesary [env variables](#env-variables)

4.  You need a server to run the app. If you want to use production server (**Not Recomended!!**), you'll need to set change serverUrl "isProduction" configuration to accept production server in development mode.

5.  Run the app

        npm run dev

At this point, you should see a screen like this:

```
> frontend@0.1.0 dev
> next dev

ready - started server on 0.0.0.0:3000, url: http://localhost:3000
info  - Loaded env from /your/path/to/kanpaiFrontend/.env.local
event - compiled client and server successfully in 5s (1300 modules)
```

## [**Env Variables**](#env-variables)

For now, there is already 3 **local variables** for frontend (The ones that have "NEXT_PUBLIC") prefix:

1. [**NEXT_PUBLIC_SERVER_URL**](.env.local): Where is stored the production server url.

2. [**NEXT_PUBLIC_LOCAL_URL**](.env.local): Where is stored the development server url => [localhost:5000](http://localhost:5000)

3. [**MAPS_API_KEY**](.env.local): We use google maps for location services, and we need a apiKey on the client side.
4. [**AWS_ACCESS_KEY_ID**](.env.local): AWS programatic access key to access database and S3 cluster.
5. [**AWS_SECRET_ACCESS_KEY**](.env.local): AWS programatic access key to access database and S3 cluster.
6. [**AWS_REGION**](.env.local): AWS region to access database and S3 cluster.
7. [**AWS_S3_BUCKET**](.env.local): AWS main S3 BUCKET
8. [**AWS_S3_BUCKET_TEMP**](.env.local): AWS main S3 temporal bucket for data sent directly from frontend.
9. [**AWS_S3_BUCKET_PUBLIC**](.env.local): AWS main S3 temporal bucket for public data.
10. [**NODE_ENV**](.env.local): This is a dev environment variable, this is not necessary in production. But it is useful for using sequelize-cli migrations and seeders in production, development or test database.
11. [**EMAIL**](.env.local): Kanpai official email for sending mails.
12. [**PASSWORD**](.env.local): Kanpai official email password
13. [**FIREBASE_CREDENTIALS**](.env.local): Firebase credentials that are necessary for firebase admin (*server side*)
14. [**DATABASE_DEV_URL**](.env.local):Url to postgresql development database.
15. [**DATABASE_PROD_URL**](.env.local):Url to postgresql testing database.
16. [**DATABASE_URL**](.env.local):Url to postgresql production database.
17. [**RECAPTCHA_SECRET_KEY**](.env.local):Key for managing and verifying captcha tokens on server side.

## [**Folder Structure**](#folder-structure)

To mantain an order in app coding, we've divided the code into various folders, each one of those with its own **README.md** file. Here, I'll _brieftly_ explain each one.

### [***Root Folder***](#root-folder)

Well, this is the main folder, in which everything is stored. All the folders and files to construct this Next.js App are here. Here are some of the root files and its explanation.

* [**.eslintrc.json:**](.eslintrc.json) Configuration file that has all the linting rules for a better experience programming in your text editor, and also for a better experience **detecting errors & warnings** in deployment phase.
* [**.gitignore:**](.gitignore) This file contains all the folders and files that are generated at development (and sometimes production) time and **CANNOT** be pushed to repo in any circumstance.
* [**next.config.js:**](next.config.js) This is a configuration file produced by Next.js to configure all its fullstack settings, for more information about that, [Click Here!](https://nextjs.org/docs/api-reference/next.config.js/introduction)
* [**package.json:**](package.json) File that stores all scripts and dependancies (_for development & production_) necessary to the app. For more info, read the [_Dependancies & Scripts_](#dependancies-&-scripts) section.

### [***.vscode***](#vscode)

The first developer of this app ([Adrien](https://github.com/frjr17)) build this app using **Visual Studio Code** text editor. In that case, _.vscode_ file contains the code snippets ([javascript.code-snippets](.vscode/javascript.code-snippets)) to agilize the coding experience. Here you'll find snippets to create a next main layout page, a panel layout page, a state function, and so on...

### [***Client Components***](#components)

Here, all the reusable components are stored. Components that are often used in _[layouts](#layouts), [pages](#pages) & [pagesComponents](#pagescomponents)_.

### [***Client Layouts***](#layouts)

There're some pages that have almost the same design structure. Therefore, layouts folder contains all the main design base, and also, it helps by adding meta tags more easily, improving SEO faster.

### [***Client Pages***](#pages)

This is a mandatory folder established by Next.js framework. Here, all pages are structured in the Next.js way. For more info about Next Routing, [Click Here!](https://nextjs.org/docs/routing/introduction)

### [***Client PagesComponents***](#pagescomponents)

This folder is a little bit different. By default, Next.js can't have multiple components in pages folder. So, this folder is created to split one page component into *multiple non-reusable* components for a better management.

### [***Client Public***](#public)

This is a **mandatory** folder provided by Next.js to store all static data: Images, svgs, favicons, and every posible static data that needs to be sent to client-side. For more info about Next Static file serving, [Click Here!](https://nextjs.org/docs/basic-features/static-file-serving)

### [***Client State***](#state)

This is almost the heart of the app. Here, all dynamic data is managed, the data in the forms, actions that happen once you click a button or sign out, and so on. State contains all the functions necessary for correct interaction with backend.

### [***Client Styles***](#styles)

This folder contains al Chakra assets necessary to use the styling library. It contains the main theme, and themes that will be used later on.

### [***Client Utils***](#utils)

Every other configuration (constants, functions, hooks, forms...) is stored in this folder.

### [***Server controllers***](controllers/README.md)
This folder is responsible for all backend services, validators and express controllers.
### [***Server database***](database/README.md)
In this folder, every database configuration (with the exception of .sequelizerc) is stored. Here are all the models, migrations and seeders for sequelize cli
### [***Server routes***](routes/README.md)
All routes (get,post,put & delete) are stored here. 
### [***Server utils***](routes/README.md)
Every other needing (constants, functions, common validators...) is here.
