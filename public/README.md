<img src="../public/images/logo/logo%20-%20s.png" alt="drawing" width="100"/>

# **_Kanpai Project - Public_** ⚒


All static data is stored here, favicons, images, svgs, and all that needs to be used in client-side.

Also, this is a [Next.js required folder](https://nextjs.org/docs/basic-features/static-file-serving).
